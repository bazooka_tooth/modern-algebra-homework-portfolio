% !TEX root = ../portfolio.tex
\noindent\textbf{10.}\quad Suppose the set \(A\) has \(n\) elements where \(n
\in \mathbb{Z}^+\)\!.

\hypertarget{1.1.10a}{\textbf{a.}\quad} How many elements does the power set
\(\mathscr{P}(A)\) have?\index{power set!number of elements}
\marginnote{\begin{defn}The \emph{power set} of a set \(A\) is the set of all
subsets of \(A\),
\begin{equation*}
	\mathscr{P}(A) = \{X\mid X\subseteq A\}.
\end{equation*}
\end{defn}}\index{power set!definition}

\noindent Choose a single element \(x\notin A\). Add this element to \(A\) to
form a new set, \(B = A\cup \{x\}\). Because the power set is the set of all
subsets, from \(\mathscr{P}(A)\) we can generate \(\mathscr{P}(B)\) as follows.
For each original element \(a\) of \(\mathscr{P}(A)\), the new element \(x\) in
\(B\) presents two new `possible subsets' of \(B\): one where \(x\) is not a
member, and one where \(x\) is a member. In other words, every element
\(a\in\mathscr{P}(A)\) will correspond to \emph{two} elements \(b_1,
b_2\in\mathscr{P}(B)\); these are \(b_1 = a\) and \(b_2 = a\cup\{x\}\).
Therefore every additional element doubles the size of the power set, and if a
set contains \(n\) elements, its power set will contain \(2^n\) elements.
%
% If we denote the number of elements in the power set of a set containing \(n\)
% elements by \(p_n\), then \(p_{n + 1} = 2p_n\) so \(p_n = 2p_{n - 1} = 2^n\). So
% if a set contains \(n\) elements, its power set will contain \(2^n\) elements.

\vspace{2ex}
\hypertarget{1.1.10b}{\textbf{b.}\quad} If \(0 \leq k \leq n\), how many
elements of the power set \(\mathscr{P}(A)\) contain exactly \(k\) elements?

Consider choosing a random sequence of \(k\) distinct elements from \(A\). At
first, one might choose any one of the \(n\) elements in \(A\). For the second
element in the sequence \(n - 1\) elements are available, for the third element
we have \(n - 2\) options, etc. Once the last (\(k^{\text{th}}\)) element is
chosen, we will have already selected \(k - 1\) of the elements from \(A\), and
so for this last element we will have \(n - (k - 1) = n - k + 1\) options.

The product of these choices are \(\prod_{i=0}^{k - 1}(k - i)\) possible
sequences of length \(k\). This is much greater than the number of possible
(unordered) sets of the same length, as we've counted every possible ordering of
the sequences. Each set of length \(k\) in \(\mathscr{P}(A)\) is represented by
\(k!\) of these sequences, because there are \(k!\) ways to order each of the
sets of length \(k\), so we should divide our count by \(k!\):\marginnote{We
find in \hyperlink{2.2.36}{exercise 36 from section 2.2} that this is the same
as \(\binom{n}{k}\).}
\begin{equation*}
	\text{There are }\frac{\prod_{i=0}^{k-1}\left(k - i\right)}{k!}\text{ sets of
	length }k\text{ in }\mathscr{P}(A).
\end{equation*}
% \footnotetext{This is the same as \(\binom{n}{k}\).}

\noindent\textbf{13.}\quad Let \(\mathbb{Z}\) denote the set of all integers,
and let\index{divisibility}
\begin{align*}
	C &= \{x \mid x = 3r - 1 \text{ for some } r \in \mathbb{Z}\}\\
	D &= \{x \mid x = 3s + 2 \text{ for some } s \in \mathbb{Z}\}.
\end{align*}
Prove that \(C = D\).\marginnote{\begin{pstrat}To show that two sets \(S\) and
\(T\) are equal, we must show that \(S\subseteq T\) and \(T\subseteq S\). More
specifically, we prove that \(x\in S\implies x\in T\) and \(x\in T\implies x\in
S\).\end{pstrat}}

\begin{proof}
	Suppose \(c\in C\), then \(c = 3r - 1\). Since \(r\in \mathbb{Z}\), \(c\) is
	one less than an integer \(3r\) that is evenly divisible by three. So the
	remainder of \(c/3\) is two.

	Because the remainder of \(c/3\) is two, \(c - 2\) must be divisible by three,
	so there exists an integer \(s\) such that \((c - 2)/3 = s\), and \(c = 3s +
	2\). By definition \(c\in D\), and we have proven that \(c \in C \implies c\in
	D\), that is, \(C\subseteq D\).

	Select an arbitrary \(d\in D\). Then \(d = 3s + 2\). Because \(s\in
	\mathbb{Z}\), \(d\) is two greater than an integer \(3s\) that is evenly
	divisible by three. So the remainder of \(d/3\) is two.

	Because the remainder of \(d/3\) is two, \(d + 1\) must be evenly divisible by
	three, so there exists an integer \(r\) such that \((d + 1)/ 3 = r\), and \(d
	= 3r - 1\). Now by definition \(d\in C\), and we have proven that \(d\in D
	\implies d\in C\), that is, \(D\subseteq C\).

	Finally, \(C \subseteq D \land D \subseteq C \implies C = D\).
\end{proof}

\noindent\textbf{40.}\quad Let the operation of addition be defined on subsets
\(A\) and \(B\) of \(U\) by \(A + B = (A\cup B)\smallsetminus(A\cap B)\). Use a
Venn diagram with labeled regions to illustrate each of the following
statements.\index{Venn diagram}
\def \setA{ (0, 0) circle (1em) }
\def \setB{ (1.1em, 0) circle (1em) }

\textbf{a.}\quad \(A + B = (A \smallsetminus B)\cup(B\smallsetminus A)\)

\noindent Let's build the left hand side using the definition of \(A + B\):
\begin{center}
	\setlength\tabcolsep{1pt}
	\begin{tabular}{ccccc}
		\((A \cup B)\) & \(\smallsetminus\) & \((A\cap B)\) &
		\(=\) & \(A + B\)\\
		\begin{tikzpicture}[baseline={([yshift=-.77ex]current bounding box.center)}]
			\fill[gray] \setA ;
			\fill[gray] \setB ;
			\draw \setA ;
			\node (alabel) at (-0.33em, 0) {\(A\)};
			\draw \setB ;
			\node (blabel) at (1.43em, 0) {\(B\)};
		\end{tikzpicture}& \(\smallsetminus\) &
		\begin{tikzpicture}[baseline={([yshift=-.77ex]current bounding box.center)}]
			\begin{scope}
				\clip \setB ;
				\fill[gray] \setA ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (-0.33em, 0) {\(A\)};
			\draw \setB ;
			\node (blabel) at (1.43em, 0) {\(B\)};
		\end{tikzpicture}& \(=\) &
		\begin{tikzpicture}[baseline={([yshift=-.77ex]current bounding box.center)}]
			\fill[gray] \setA ;
			\fill[gray] \setB ;
			\begin{scope}
				\clip \setB ;
				\fill[white] \setA ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (-0.33em, 0) {\(A\)};
			\draw \setB ;
			\node (blabel) at (1.43em, 0) {\(B\)};
		\end{tikzpicture}.
	\end{tabular}
\end{center}
For the right hand side, we have:
\begin{center}
	\setlength\tabcolsep{1pt}
	\begin{tabular}{ccccc}
		\((A \smallsetminus B)\) & \(\cup\) & \((B\smallsetminus A)\) &
		\(=\) & \(A + B\)\\
		\begin{tikzpicture}[baseline={([yshift=-.77ex]current bounding box.center)}]
			\fill[gray] \setA ;
			\begin{scope}
				\clip \setA ;
				\fill[white] \setB ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (-0.33em, 0) {\(A\)};
			\draw \setB ;
			\node (blabel) at (1.43em, 0) {\(B\)};
		\end{tikzpicture}& \(\cup\) &
		\begin{tikzpicture}[baseline={([yshift=-.77ex]current bounding box.center)}]
			\fill[gray] \setB ;
			\begin{scope}
				\clip \setB ;
				\fill[white] \setA ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (-0.33em, 0) {\(A\)};
			\draw \setB ;
			\node (blabel) at (1.43em, 0) {\(B\)};
		\end{tikzpicture}& \(=\) &
		\begin{tikzpicture}[baseline={([yshift=-.77ex]current bounding box.center)}]
			\fill[gray] \setA ;
			\fill[gray] \setB ;
			\begin{scope}
				\clip \setB ;
				\fill[white] \setA ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (-0.33em, 0) {\(A\)};
			\draw \setB ;
			\node (blabel) at (1.43em, 0) {\(B\)};
		\end{tikzpicture}.
	\end{tabular}
\end{center}

\vspace{2ex}
\def \setA{ (0.55em, 0.91em) circle (1em) }
\def \setB{ (0, 0) circle (1em) }
\def \setC{ (1.1em, 0) circle (1em) }
\hypertarget{1.1.40b}{\textbf{b.}\quad}
\(A + (B + C) = (A + B) + C\)\marginnote{See \hyperlink{3.1.36a.associative}
{exercise 36 part \textbf{a} in section 3.1} for an algebraic proof of this
fact, and see \hyperlink{review2.4.associative}{problem 4 in the review for test
2} for a proof utilizing the characteristic function of a set.}

\noindent For \((B + C)\) in the left hand side, using the given definition of
addition,
\begin{center}
	\setlength\tabcolsep{1pt}
	\begin{tabular}{ccccc}
		\((B\cup C)\) & \(\smallsetminus\) & \((B\cap C)\) & \(=\) & \(B + C\)\\
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\fill[gray] \setB ;
			\fill[gray] \setC ;
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}& \(\smallsetminus\) &
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\begin{scope}
				\clip \setB ;
				\fill[gray] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}& \(=\) &
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\fill[gray] \setB ;
			\fill[gray] \setC ;
			\begin{scope}
				\clip \setB ;
				\fill[white] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}.
	\end{tabular}
\end{center}
Now \(A + (B + C) = \left(A\cup (B + C)\right)\smallsetminus\left(A\cap(B + C)
\right)\), so for the entire left hand side we have:
\begin{center}
	\setlength\tabcolsep{1pt}
	\begin{tabular}{cccccccc}
		\((\quad\enspace A\quad\) & \(\cup\) & \((B + C))\) &
		\(\smallsetminus\) & \((\quad\enspace A\quad\) & \(\cap\) & \((B + C))\)&\\
		\(\left(\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
			box.center)}]
			\fill[gray] \setA ;
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right.\)&\(\cup\)&
		\(\left.\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
		box.center)}]
			\fill[gray] \setB ;
			\fill[gray] \setC ;
			\begin{scope}
				\clip \setB ;
				\fill[white] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right)\)&\(\smallsetminus\)&
		\(\left(\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
			box.center)}]
			\fill[gray] \setA ;
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right.\)&\(\cap\)&
		\(\left.\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
		box.center)}]
			\fill[gray] \setB ;
			\fill[gray] \setC ;
			\begin{scope}
				\clip \setB ;
				\fill[white] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right)\)&\(=\)
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\fill[gray] \setB ;
			\fill[gray] \setC ;
			\begin{scope}
				\clip \setB ;
				\fill[white] \setC ;
			\end{scope}
			\fill[gray] \setA ;
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\(\smallsetminus\)
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\begin{scope}
				\clip \setA ;
				\fill[gray] \setB ;
				\fill[gray] \setC ;
			\end{scope}
			\begin{scope}
				\clip \setB ;
				\fill[white] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\(=\)
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\fill[gray] \setA ;
			\fill[gray] \setB ;
			\fill[gray] \setC ;
			\begin{scope}
				\clip \setA ;
				\fill[white] \setB ;
			\end{scope}
			\begin{scope}
				\clip \setB ;
				\fill[white] \setC ;
			\end{scope}
			\begin{scope}
				\clip \setC ;
				\fill[white] \setA ;
			\end{scope}
			\begin{scope}
				\clip \setA ;
				\clip \setB ;
				\fill[gray] \setC;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}.
	\end{tabular}
\end{center}
For \((A + B)\) in the right hand side:
\begin{center}
	\setlength\tabcolsep{1pt}
	\begin{tabular}{ccccc}
		\((A\cup B)\) & \(\smallsetminus\) & \((A\cap B)\) & \(=\) & \(A + B\)\\
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\fill[gray] \setA ;
			\fill[gray] \setB ;
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}& \(\smallsetminus\) &
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\begin{scope}
				\clip \setA ;
				\fill[gray] \setB ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}& \(=\) &
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\fill[gray] \setA ;
			\fill[gray] \setB ;
			\begin{scope}
				\clip \setA ;
				\fill[white] \setB ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}.
	\end{tabular}
\end{center}
Now \((A + B) + C = ((A + B)\cup C)\smallsetminus ((A + B)\cap C)\), so for the
entire right hand side we obtain
\begin{center}
	\setlength\tabcolsep{1pt}
	\begin{tabular}{cccccccc}
		\(((A + B)\) & \(\cup\) & \(\quad\enspace C\quad)\)
		& \(\smallsetminus\) & \(((A + B)\) & \(\cap\) &
		\(\quad\enspace C\quad)\) & \\
		\(\left(\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
			box.center)}]
			\fill[gray] \setA ;
			\fill[gray] \setB ;
			\begin{scope}
				\clip \setA ;
				\fill[white] \setB ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right.\)&\(\cup\)&
		\(\left.\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
		box.center)}]
			\fill[gray] \setC ;
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right)\)&\(\smallsetminus\)&
		\(\left(\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
			box.center)}]
			\fill[gray] \setA ;
			\fill[gray] \setB ;
			\begin{scope}
				\clip \setA ;
				\fill[white] \setB ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right.\)&\(\cap\)&
		\(\left.\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
		box.center)}]
			\fill[gray] \setC ;
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right)\)&\(=\)
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\fill[gray] \setA ;
			\fill[gray] \setB ;
			\begin{scope}
				\clip \setA ;
				\fill[white] \setB ;
			\end{scope}
			\fill[gray] \setC ;
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\(\smallsetminus\)
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\begin{scope}
				\clip \setA ;
				\fill[gray] \setC ;
			\end{scope}
			\begin{scope}
				\clip \setC ;
				\fill[gray] \setB ;
			\end{scope}
			\begin{scope}
				\clip \setA ;
				\fill[white] \setB ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\(=\)
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\fill[gray] \setA ;
			\fill[gray] \setB ;
			\fill[gray] \setC ;
			\begin{scope}
				\clip \setA ;
				\fill[white] \setB ;
			\end{scope}
			\begin{scope}
				\clip \setB ;
				\fill[white] \setC ;
			\end{scope}
			\begin{scope}
				\clip \setC ;
				\fill[white] \setA ;
			\end{scope}
			\begin{scope}
				\clip \setA ;
				\clip \setB ;
				\fill[gray] \setC;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}.
	\end{tabular}
\end{center}

\vspace{2ex}
\textbf{c.}\quad \(A \cap (B + C) = (A \cap B) + (A \cap C)\)

\noindent Using \((B + C)\) from part \textbf{b}, the left hand side is
\begin{center}
	\setlength\tabcolsep{1pt}
	\begin{tabular}{cccc}
		\(A\) & \(\cap\) & \((B + C)\) & \\
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\fill[gray] \setA ;
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture} & \(\cap\) &
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\fill[gray] \setB ;
			\fill[gray] \setC ;
			\begin{scope}
				\clip \setB ;
				\fill[white] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}& \(=\)
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\begin{scope}
				\clip \setA ;
				\fill[gray] \setB ;
				\fill[gray] \setC ;
			\end{scope}
			\begin{scope}
				\clip \setB ;
				\fill[white] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}.
	\end{tabular}
\end{center}
For the right hand side,
\begin{equation*}
	A \cap B =
	\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
		\begin{scope}
			\clip \setA ;
			\fill[gray] \setB ;
		\end{scope}
		\draw \setA ;
		\node (alabel) at (0.55em, 1.4em) {\(A\)};
		\draw \setB ;
		\node (blabel) at (-0.4em, -0.3em) {\(B\)};
		\draw \setC ;
		\node (clabel) at (1.4em, -0.3em) {\(C\)};
	\end{tikzpicture}
	\text{\quad and\quad}A\cap C =
	\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
		\begin{scope}
			\clip \setA ;
			\fill[gray] \setC ;
		\end{scope}
		\draw \setA ;
		\node (alabel) at (0.55em, 1.4em) {\(A\)};
		\draw \setB ;
		\node (blabel) at (-0.4em, -0.3em) {\(B\)};
		\draw \setC ;
		\node (clabel) at (1.4em, -0.3em) {\(C\)};
	\end{tikzpicture},
\end{equation*}
and \((A\cap B) + (A\cap C)\) is by definition
\begin{center}
	\setlength\tabcolsep{1pt}
	\begin{tabular}{cccccccc}
		\(((A\cap B)\) & \(\cup\) & \((A\cap C))\) & \(\smallsetminus\) &
		\(((A\cap B)\) & \(\cap\) & \((A\cap C))\) & \\
		\(\left(\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
			box.center)}]
			\begin{scope}
				\clip \setA ;
				\fill[gray] \setB ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right.\) & \(\cup\) &
		\(\left.\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
			box.center)}]
			\begin{scope}
				\clip \setA ;
				\fill[gray] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right)\) & \(\smallsetminus\) &
		\(\left(\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
			box.center)}]
			\begin{scope}
				\clip \setA ;
				\fill[gray] \setB ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right.\) & \(\cap\) &
		\(\left.\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding
			box.center)}]
			\begin{scope}
				\clip \setA ;
				\fill[gray] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\right)\) & =
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\begin{scope}
				\clip \setA ;
				\fill[gray] \setB ;
				\fill[gray] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\(\smallsetminus\)
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\begin{scope}
				\clip \setA ;
				\clip \setB ;
				\fill[gray] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}\(=\)
		\begin{tikzpicture}[baseline={([yshift=-.85ex]current bounding box.center)}]
			\begin{scope}
				\clip \setA ;
				\fill[gray] \setB ;
				\fill[gray] \setC ;
			\end{scope}
			\begin{scope}
				\clip \setB ;
				\fill[white] \setC ;
			\end{scope}
			\draw \setA ;
			\node (alabel) at (0.55em, 1.4em) {\(A\)};
			\draw \setB ;
			\node (blabel) at (-0.4em, -0.3em) {\(B\)};
			\draw \setC ;
			\node (clabel) at (1.4em, -0.3em) {\(C\)};
		\end{tikzpicture}.
	\end{tabular}
\end{center}
