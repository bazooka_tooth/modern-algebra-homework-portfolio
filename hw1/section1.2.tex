% !TEX root = ../portfolio.tex
\marginnote{\begin{defn}
	A subset \(f\) of \(A\times B\) is a \emph{mapping} from \(A\) to \(B\),
	denoted \(f\colon A\to B\), iff for every \(a\in A\) there is a unique
	\(b\in B\) s.t.\ \((a,b)\in f\).
\end{defn}}\index{mapping!definition}
\marginnote{\begin{defn}
	If \(f\colon A\to B\), then \(A\) is the \emph{domain} of \(f\), \(B\) is the
	\emph{codomain}, and
	\begin{equation*}
		f(A) = \{y\mid y\in B\text{ and }y = f(x)\text{ for some }x\in A\}
	\end{equation*}
	is the \emph{range}.
\end{defn}}\index{mapping!domain/codomain/range}

\noindent\textbf{5.}\quad For each of the following mappings\index{mapping}
\(f\colon\mathbb{R}\to\mathbb{R}\), determine whether the mapping is onto and
whether it is one-to-one. Justify all negative answers. (Compare these results
with the corresponding parts of Exercise 4.)

\textbf{c.}\quad \(f(x) = x + 3\).

\noindent Choose an arbitrary element from the codomain, \(y\in\mathbb{R}\).
Then the value \(x\) that satisfies \(y = x + 3\), that is, \(y - 3\), will be
a real number. So the codomain is equal to the range,
\begin{equation*}
	\mathbb{Z} = \{y\mid y\in\mathbb{R}\text{ and }y = x + 3\text{ for
	some }x\in\mathbb{R}\},
\end{equation*}
and \(f\) is onto.
Choose two elements from the domain, \(a_1, a_2\in\mathbb{R}\).
Assume that \(f(a_1) = f(a_2)\). Then \(a_1 + 3 = a_2 + 3\implies a_1 = a_2\),
so \(f\) is one-to-one.
\marginnote[-1.8\baselineskip]{\begin{defn}
	A function \(f\) is \emph{onto} \(B\) if and only if \(B = f(A)\);
	\emph{surjective} is a synonym of `onto'.
\end{defn}}\index{mapping!onto}
\marginnote{\begin{pstrat}
	To show that a function is onto, we show that every element of the codomain
	is mapped to by some element of the domain.
\end{pstrat}}

\vspace{2ex}
\textbf{d.}\quad \(f(x) = x^3\).

\noindent Again, for any element in the codomain \(y\in\mathbb{R}\), the value
\(x\) that satisfies \(y = x^3\), that is, \(\sqrt[3]{y}\), is a real number,
therefore \(f\) is onto. Given two elements from the domain \(a_1,a_2\in
\mathbb{R}\), \(f(a_1) = f(a_2) \implies a_1^3 = a_2^3 \implies a_1 = a_2\), so
\(f\) is one-to-one.

\marginnote{\begin{defn}
	A function \(f\) is \emph{one-to-one} if and only if \(a_1\neq a_2\implies
	f(a_1)\neq f(a_2)\) for all \(a_1,a_2\in A\); \emph{injective} is a synonym
	of `one-to-one'.
\end{defn}}\index{mapping!one-to-one}
\marginnote{\begin{pstrat}
	It is usually easiest to prove that a function is one-to-one by
	contrapositive, i.e. \(f(a_1) = f(a_2) \implies a_1 = a_2\).
\end{pstrat}}
\marginnote{\begin{defn}
	A function is a \emph{one-to-one correspondence} if it is both onto and
	one-to-one; \emph{bijective} is a synonym of `one-to-one correspondence'.
\end{defn}}\index{mapping!one-to-one correspondence}
In Exercise 4, \(f\colon\mathbb{Z}\to\mathbb{Z}\). This affects only the
surjectivity of this function; observe that two is in the codomain, \(2\in
\mathbb{Z}\), but there does not exist an \(x\) in the domain, \(x\in
\mathbb{Z}\) such that \(2 = x^3\), therefore in this case \(f\) is not
onto.

\vspace{2ex}
\noindent\textbf{12.}\quad Let \(A = \mathbb{R}\smallsetminus \{0\}\) and \(B =
\mathbb{R}\). For the given \(f\colon A\to B\), determine whether \(f\) is onto
and whether it is one-to-one. Prove that your decisions are correct.

\textbf{b.}\quad \(f(x) = (2x - 1)/\!x\).

% \noindent The function \(f\) is not onto because two is in the codomain, but two
% is not in the range.
\begin{proof}[The function \(f\) is not onto]
	Observe that two is an element of the codomain, \(2\in\mathbb{R}\), and
	consider the preimage element \(x\) that must map to \(y = 2\) if two is in
	the range. If \(2 = (2x - 1)/\!x\) then \(2x = 2x - 1\) and \(0 = -1\), a
	contradiction. Therefore two is not an element of the range, \(\mathbb{R}\neq
	f(\mathbb{R}\smallsetminus\{0\})\), and \(f\) is not onto.
\end{proof}

\begin{proof}[The function \(f\) is one-to-one]
	Choose two elements from the domain, \(a_1,a_2\in\mathbb{R}
	\smallsetminus\{0\}\), and assume that \(f(a_1) = f(a_2)\). Then
	\begin{alignat*}{2}
		&& f(a_1) &= f(a_2)\\
		&\implies& \frac{2a_1 - 1}{a_1} &= \frac{2a_2 - 1}{a_2}\\
		&\implies& \left(2a_2 - 1\right)a_1 &= \left(2a_1 - 1\right)a_2\\
		&\implies& 2a_1a_2 - a_1 &= 2a_1a_2 - a_2\\
		&\implies& -a_1 &= -a_2\\
		&\implies& a_1 &= a_2.
	\end{alignat*}
	Therefore \(f\) is one-to-one.
\end{proof}

\textbf{c.}\quad \(f(x) = x/\!(x^2 + 1)\).

\begin{proof}[The function above is not onto]
	The codomain contains zero, so assuming that \(f\) is onto (i.e.\ \(B =
	f(A)\)), then for some \(x\in A\), \(0 = x/\!(x^2 + 1)\). This equation has
	only one solution, \(x = 0\), but\marginnote{Even if \(A\) did contain
	zero, then \(f\) still would not be onto, since
	\begin{equation*}
		f(A) = [-1/2, 1/2] \neq B.
	\end{equation*}} \(0\notin A\). Therefore \(0\notin f(A)\),
	so \(B\neq f(A)\), and by definition \(f\) is not onto.
\end{proof}

\begin{proof}[The function above is not one-to-one]
	Choose an arbitrary \(a_1\in A\) and let \(a_2 = 1/a_1\). Assume \(f\) is
	one-to-one, further, assume \(a_1\neq a_2\) (that is, \(a_1,a_2\neq 1\)). Then
	\(f(a_1) = \frac{a_1}{a_1^2 + 1}\), and \(f(a_2) = \frac{a_2}{a_2^2 + 1} =
	\frac{1/a_1}{1/a_1^2 + 1}\). If \(f\) is one-to-one, \(\forall a_1,a_2\in A,
	a_1\neq a_2 \implies f(a_1) \neq f(a_2)\), so
	\begin{alignat*}{2}
		&& f(a_1) &\neq f(a_2)\\
		&\implies& \frac{a_1}{a_1^2 + 1} &\neq \frac{\frac{1}{a_1}}{\frac{1}{a_1^2}
		+ 1}\\
		&\implies& a_1\left(\frac{1}{a_1^2} + 1\right) &\neq \frac{1}{a_1}
		\left(a_1^2 + 1\right)\\
		&\implies& \frac{1}{a_1} + a_1 &\neq a_1 + \frac{1}{a_1}\\
		&\implies& a_1 &\neq a_1.
	\end{alignat*}
	We have arrived at a contradiction, so \(f\) is not one-to-one.
\end{proof}
