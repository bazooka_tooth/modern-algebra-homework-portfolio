# Modern Algebra Homework Portfolio

This repository is the LaTeX source of a final project for Dr. Ahangar's Spring
2018 MATH 4340 (Modern Algebra) class. It consists of collected solutions for
all homework assignments, a review for the second test, and solutions for two
problems from each test.

The portfolio may be typeset by running the command

    pdflatex portfolio.tex

from this directory. It may be necessary to run the command a second (or even
third) time, in order to completely build cross-references, hyperlinks, etc.
This project depends on many LaTeX packages; if pdflatex informs you that a
package was not found, then that package should freely obtainable at
https://ctan.org.

When typesetting the project for physical printing, the second line in
portfolio.tex (the line containing "\hypersetup{colorlinks}") should be
commented out. This will disable the blue text color of hyperlinks in the PDF
output. If the project is to be typeset for on-screen viewing, this line should
be left uncommented.

After the portfolio.pdf file has been built, a booklet can be generated with the
command

    pdflatex booklet.tex

from this directory. This will produce a file that, when duplex printed (i.e.
printed front and back), will result in a document that can be folded in half
and stapled to form a small booklet. The font size in this booklet will be
extremely small!

This project makes use of parts of the Tufte-LaTeX package, version 3.5.2, which
is available in its original form at https://tufte-latex.github.io/tufte-latex/.
Specifically, the Tufte-LaTeX files

    'tufte-book.cls' and
    'tufte-common.def'

have been used. To these two files  the following licensing information applies:

    Copyright 2007–2015 by Kevin Godby, Bil Kleb, and Bill Wood.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

The remainder of this work (that is, excluding the two files 'tufte-book.cls'
and 'tufte-common.def') is copyright 2018 by Lane Christiansen, and is licensed
under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to Creative
Commons, PO Box 1866, Mountain View, CA 94042, USA.
