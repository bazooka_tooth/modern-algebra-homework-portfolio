% !TEX root = ../portfolio.tex
\noindent\textbf{5.}\quad \hypertarget{test2review.5}{Suppose} \(R\) is a ring
with unity \(e\).
\index{ring!unity}
\marginnote{\begin{defn}
  Let \(R\) be a ring. If there exists an \(e\in R\) such that \(x\cdot e =
  e\cdot x = x\) for all \(x\in R\), then \(e\) is a \emph{unity}, and \(R\)
  is a \emph{ring with unity}.

  Note that the multiplication associated with a ring is not necessarily
  commutative, but by this definition, in a ring with unity \(e\),
  multiplication by \(e\) is \emph{always} commutative. If \emph{all} elements
  of a ring commute under multiplication then it is a \emph{commutative ring}.
  In any case, the addition associated with a ring is always commutative.
\end{defn}}

\textbf{a.}\quad Prove that the unity \(e\) is unique.
\begin{proof}
  Suppose that \(e'\) is also a unity. Since \(e\) is a unity, by the definition
  above, \(e'\cdot e = e\cdot e' = e'\). Since \(e'\) is also a unity, by the
  same definition, \(e\cdot e' = e'\cdot e = e\), therefore \(e = e'\) and the
  unity is unique.\footnote{This proof is very similar to the proof of the
  textbook's theorem 5.6.}
\end{proof}

\textbf{b.}\quad Prove that \(\forall a\in R\), \(a\cdot 0 = 0\cdot
a = 0\).
\begin{proof}
  We mention the conditions of the definition of a ring that are used to prove
  the statement, noting that because \(R\) is closed under multiplication,
  \(a\cdot 0\) and \(0\cdot a\) are also in \(R\). First,
  \begin{alignat*}{2}
    a\cdot0 &= a\cdot0 + 0&&\quad\text{by the existence of an additive
    identity}\\
    &= a\cdot0 + [a\cdot0 + (-(a\cdot0))]&&\quad\text{by the existence of
    additive inverses}\\
    &= (a\cdot0 + a\cdot0) + (-(a\cdot0))&&\quad\text{by the associativity of
    the addition}\\
    &= a\cdot(0+0)+(-(a\cdot0))&&\quad\text{by the left distributive law}\\
    &= a\cdot0 + (-(a\cdot0))&&\quad\text{by the existence of an additive
    identity}\\
    &= 0&&\quad\text{by the existence of additive inverses,}
  \end{alignat*}
  therefore \(\forall a\in R\), \(a\cdot0=0\). Now,
  \begin{alignat*}{2}
    0\cdot a &= 0 + 0\cdot a&&\quad\text{by the existence of an additive
    identity}\\
    &= [(-(0\cdot a)) + 0\cdot a] + 0\cdot a&&\quad\text{by the existence of
    additive inverses}\\
    &= (-(0\cdot a)) + (0\cdot a + 0\cdot a)&&\quad\text{by the associativity
    of the addition}\\
    &= (-(0\cdot a)) + (0+0)\cdot a&&\quad\text{by the right distributive
    law}\\
    &= (-(0\cdot a)) + 0\cdot a&&\quad\text{by the existence of an additive
    identity}\\
    &= 0&&\quad\text{by the existence of additive inverses,}
  \end{alignat*}
  therefore \(\forall a\in R\), \(0\cdot a = 0\), and furthermore \(a\cdot0
  = 0 = 0\cdot a\).\footnote{This doesn't depend on the unity \(e\), so this
  statement holds for any ring. See the following theorem.}
  \marginnote{\begin{thm}
    If \(R\) is a ring then
    \begin{equation*}
      \forall a\in R\enspace a\cdot0=0\cdot a = 0.
    \end{equation*}
  \end{thm}}
\end{proof}
