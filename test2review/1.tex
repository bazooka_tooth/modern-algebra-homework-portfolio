% !TEX root = ../portfolio.tex
\vspace{2ex}
\noindent\textbf{1.}\quad Let \(\mathbb{Z}\) be the set of integers.
\marginnote{See the postulates for the integers at \hyperlink{zpostulates}{the
beginning of the third chapter} of this text, and the definition of a group at
\hyperlink{groupdef}{the beginning of the sixth chapter} of this text.}

\index{z@\(\mathbb{Z}\)!group|(}
\textbf{a.}\quad Show that \((\mathbb{Z},+)\) is a commutative group.

\noindent That \((\mathbb{Z},+)\) is a commutative group follows from the
postulates for \(\mathbb{Z}\):
\begin{enumerate}[nolistsep]
  \item \(\mathbb{Z}\) is closed under addition; \(\forall x,y\in\mathbb{Z}\)
  \enspace\((x+y)\in\mathbb{Z}\).
  \item Addition of integers is associative; \(\forall x,y,z\in\mathbb{Z}\)
  \enspace\(x+(y+z)=(x+y)+z\).
  \item \(\mathbb{Z}\) has an additive identity element, zero; \(\forall
  x\in\mathbb{Z}\)\enspace\(x+0=0+x=x\).
  \item \(\mathbb{Z}\) contains additive inverses; \(\forall x\in\mathbb{Z}\)
  \enspace\(\exists(-x)\in\mathbb{Z}\) such that \(x+(-x)=0=(-x)+x\).
  \item Addition of integers is commutative; \(\forall x,y\in\mathbb{Z}\)
  \enspace\(x+y=y+x\).
\end{enumerate}
The first four facts make \((\mathbb{Z},+)\) a group. Since the fifth is also
satisfied, \((\mathbb{Z},+)\) is a commutative or \emph{abelian} group.

\vspace{2ex}
\textbf{b.}\quad Show that \((\mathbb{Z},\cdot)\) is a semigroup.

\noindent That \((\mathbb{Z},\cdot)\) is a semigroup\marginnote{In fact, since
\(\mathbb{Z}\) contains a multiplicative identity and multiplication of integers
is commutative, \((\mathbb{Z},\cdot)\) is also a \emph{commutative monoid}.}
follows from the postulates
for \(\mathbb{Z}\):
\begin{enumerate}[nolistsep]
  \item \(\mathbb{Z}\) is closed under multiplication; \(\forall x,y\in
  \mathbb{Z}\)\enspace\((xy)\in\mathbb{Z}\).
  \item Multiplication of integers is associative; \(\forall x,y,z\in
  \mathbb{Z}\)\enspace\(x(yz)=(xy)z\).
\end{enumerate}\index{z@\(\mathbb{Z}\)!group|)}

\vspace{2ex}\index{z@\(\mathbb{Z}\)!ring|(}
\textbf{c.}\quad Explain why the triple \((\mathbb{Z},+,\cdot)\) is a
ring.\index{ring!alternate definition}\marginnote{\begin{defn}
  \hypertarget{ringalternate}{A set} \(R\) in which a relation of equality is
  defined is a \emph{ring} with respect to predefined operations of addition, \(+\),
  and multiplication, \(\cdot\), iff:
  \begin{enumerate}[nolistsep]
    \item \((R,+)\) is an \emph{abelian group},
    \item \(R\) is \emph{closed} with respect to an \emph{associative
    multiplication}, and
    \item Two \emph{distributive laws} hold in \(R\), i.e.\ \(\forall x,y,z\in
    R,\enspace x\cdot(y+z)=x\cdot y+x\cdot z\) and \((x+y)\cdot z = x\cdot z +
    y\cdot z\).
  \end{enumerate}
  This is an alternative definition of a ring, see also the
  \hyperlink{ringfull}{full definition} in the margin of problem 4, below.
\end{defn}}\index{z@\(\mathbb{Z}\)!ring|)}

\noindent\((\mathbb{Z},+,\cdot)\) is a ring, since:
\begin{enumerate}[nolistsep]
  \item \(\mathbb{Z}\) forms a commutative group under addition.
  \item Multiplication in \(\mathbb{Z}\) is closed and associative (that is,
  \((\mathbb{Z},\cdot)\) is a semigroup).
  \item The left distributive and right distributive laws hold;
  \(\forall x,y,z\in\mathbb{Z}\)\enspace\(x(y+z)=xy+xz\) and \((x+y)z=xz+yz\).
\end{enumerate}
