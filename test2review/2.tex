% !TEX root = ../portfolio.tex
\vspace{2ex}
\noindent\textbf{2.}\quad Let \(\mathbb{Q}\) represent the set of all rational
numbers.

\index{q@\(\mathbb{Q}\)!group|(}
\textbf{a.}\quad Verify that \((\mathbb{Q},+)\) is a commutative group.
\begin{proof}[\hypertarget{qclosedadd}{\(\mathbb{Q}\) is closed under addition}]
  Let \(x,y\in\mathbb{Q}\). Then \(x=a/b\) and \(y=c/d\) for some \(a,b,c,d\in
  \mathbb{Z}\) with \(b\neq0\) and \(d\neq0\). Then
  \begin{equation*}
    x+y = \frac{a}{b} + \frac{c}{d} = \frac{ad}{bd} + \frac{bc}{bd} =
    \frac{ad+bc}{bd}.
  \end{equation*}
  Since \((\mathbb{Z},+,\cdot)\) is a ring, it follows that \(ad+bc\in
  \mathbb{Z}\) and \(bd\in\mathbb{Z}\), and since \(b\neq0\) and \(d\neq0\)
  the denominator \(bd\) must be nonzero, so \(x+y\in\mathbb{Q}\). Therefore
  \(\forall x,y\in\mathbb{Q}\)\enspace\(x+y\in\mathbb{Q}\).
\end{proof}
\begin{proof}[Addition of rational numbers is associative]
  Let \(x,y,z\in\mathbb{Q}\). Then \(x=a/b\), \(y=c/d\), and \(z=e/f\) for some
  \(a,b,c,d,e,f\in\mathbb{Q}\) with \(b\neq0\), \(d\neq0\), and
  \(f\neq0\). Then
  \begin{multline*}
    x+(y+z) = \frac{a}{b}+\left(\frac{c}{d}+\frac{e}{f}\right) = \frac{a}{b} +
    \frac{cf+de}{df} = \frac{adf + bcf + bde}{bdf} \\= \frac{ad\cancel{f} +
    bc\cancel{f}}{bd\cancel{f}} + \frac{\cancel{bd}e}{\cancel{bd}f} =
    \frac{ad + bc}{bd} + \frac{e}{f} = \left(\frac{a}{b}+\frac{c}{d}\right) +
    \frac{e}{f} = (x+y)+z.
  \end{multline*}
\end{proof}
\begin{proof}[\(\mathbb{Q}\) has an additive identity element, zero]
  For all \(x\in\mathbb{Q}\), \(x+0=0+x=x\), so zero is the identity element for
  \(\mathbb{Q}\) under addition.
\end{proof}
\begin{proof}[\(\mathbb{Q}\) contains additive inverses]
  Let \(x\in\mathbb{Q}\), then \(x=a/b\) for some \(a,b\in\mathbb{Z}\) with
  \(b\neq0\). Since \(\mathbb{Z}\) contains additive inverses, there exists a
  \(-a\in\mathbb{Z}\) such that \(a + (-a) = 0 = (-a) + a\), so
  \begin{equation*}
    \frac{a}{b} + \frac{-a}{b} = \frac{a + (-a)}{b} = 0
    = \frac{(-a) + a}{b} = \frac{-a}{b} + \frac{a}{b},
  \end{equation*}
  therefore \(\mathbb{Q}\) contains additive inverses.
\end{proof}
\begin{proof}[Addition of rational numbers is commutative]
  Let \(x,y\in\mathbb{Q}\). Then \(x=a/b\) and \(y=c/d\) for some \(a,b,c,d\in
  \mathbb{Z}\) with \(b\neq0\) and \(d\neq0\). Then we can write the same
  expression from the \emph{``\hyperlink{qclosedadd}{\(\mathbb{Q}\) is closed
  under addition}''} proof above, that is, \(x+y = (ad+bc)/bd\). Now
  \(ad\in\mathbb{Z}\), \(bc\in \mathbb{Z}\), and addition of integers is
  commutative, so
  \begin{equation*}
    x+y = \frac{bc + ad}{bd} = \frac{\cancel{b}c}{\cancel{b}d}
    + \frac{a\cancel{d}}{b\cancel{d}} = y + x,
  \end{equation*}
  therefore for all \(x,y\in\mathbb{Q}\), \(x+y = y+x\).
\end{proof}

\textbf{b.}\quad Verify that \((\mathbb{Q},\cdot)\) is a commutative
group.\marginnote{\(\mathbb{Q}\) is a commutative group only if zero is
excluded. I've left this problem as it is on the review sheet, see the
`\hyperlink{qinverses}{inverse element}' part below.}

\begin{proof}[\(\mathbb{Q}\) is closed under multiplication]
  Let \(x,y\in\mathbb{Q}\). Then \(x=a/b\) and \(y=c/d\) for some \(a,b,c,d\in
  \mathbb{Z}\) with \(b\neq0\) and \(d\neq0\). Then
  \begin{equation*}
    x\cdot y = \frac{a}{b}\cdot\frac{c}{d} = \frac{ac}{bd}.
  \end{equation*}
  Since \(\mathbb{Z}\) is closed under addition, \((ac)/(bd)\) is a quotient
  of the integers \((ac)\) and \((bd)\), and since \(b\neq0\) and \(d\neq0\),
  \(bd\neq0\). Therefore this quotient is a rational number, that is,
  \(x\cdot y\in\mathbb{Q}\). The choices of \(x\) and \(y\) are arbitrary, so
  \(\forall x,y\in\mathbb{Q}\) \(x\cdot y\in\mathbb{Q}\).
\end{proof}

\begin{proof}[Multiplication of rational numbers is associative]
  Let \(x,y,z\in\mathbb{Q}\). Then \(x=a/b\), \(y=c/d\), and \(z=e/f\) with
  \(b\neq0\), \(d\neq0\), and \(f\neq0\). Then
  \begin{equation*}
    x\cdot(y\cdot z) = \frac{a}{b}\cdot\left(\frac{c}{d}\cdot\frac{e}{f}
    \right) = \frac{a}{b}\cdot\frac{ce}{df}=\frac{ace}{bdf} = \frac{ac}{bd}
    \cdot\frac{e}{f} = \left(\frac{a}{b}\cdot\frac{c}{d}\right)\cdot\frac{e}
    {f}= (x\cdot y)\cdot z,
  \end{equation*}
  that is, the associativity of multiplication in \(\mathbb{Q}\) follows from
  the associativity of multiplication in \(\mathbb{Z}\).
\end{proof}

\begin{proof}[\(\mathbb{Q}\) has a multiplicative identity element, one]
  Let \(x\in\mathbb{Q}\). Then \(x=a/b\) for some \(a,b\in\mathbb{Z}\) with
  \(b\neq0\). Then
  \begin{equation*}
    x\cdot1 = \frac{a}{b}\cdot1 = \frac{a}{b}\cdot\frac{1}{1} = \frac{a\cdot1}
    {b\cdot1} = \frac{1\cdot a}{1\cdot b} = \frac{1}{1}\cdot\frac{a}{b}
    = 1\cdot\frac{a}{b} = 1\cdot x,
  \end{equation*}
  and
  \begin{equation*}
    x\cdot1 = \frac{a}{b}\cdot1 = \frac{a}{b}\cdot\frac{1}{1} = \frac{a\cdot1}
    {b\cdot1} = \frac{a}{b} = x,
  \end{equation*}
  that is, that one is the multiplicative identity in \(\mathbb{Q}\) follows
  from the commutativity of multiplication in \(\mathbb{Z}\) and the fact that
  one is the multiplicative identity in \(\mathbb{Z}\). Therefore, for all
  \(x\in\mathbb{Q}\), \(x\cdot1 = 1\cdot x = x\).
\end{proof}

\begin{proof}[\hypertarget{qinverses}{\(\mathbb{Q}\smallsetminus\{0\}\)
  contains\nopunct}]
  \hspace{-0.25em}\emph{inverses.}\marginnote{There is no inverse element for
  zero. Although it could be that \(a=0\) and \(b\) could be any integer, then
  \(x\) would be zero, but \(x^{-1}=b/a\) would not be well-defined, since
  \((a\cdot b)/(b\cdot a)\) would be equal to \(0/0\), not one.}
  Let \(x\in\mathbb{Q}\smallsetminus\{0\}\). Then \(x=a/b\) for some \(a,b
  \in\mathbb{Z}\) with \(b\neq0\), and since \(x\neq0\), \(a\neq0\). Then
  the quotient \(b/a\in\mathbb{Q}\smallsetminus\{0\}\), and this quotient
  is the inverse element of \(x\), which is denoted by \(x^{-1}\):
  \begin{equation*}
    x\cdot x^{-1} = \frac{a}{b}\cdot\frac{b}{a} = \frac{a\cdot b}{b\cdot a}
    = \frac{a\cdot b}{a\cdot b} = 1.
  \end{equation*}
  Therefore \(\forall x\in\mathbb{Q}\smallsetminus\{0\}\) \(\exists x^{-1}
  \in\mathbb{Q}\smallsetminus\{0\}\) such that \(x\cdot x^{-1} = 1 = x^{-1}
  \cdot x\).
\end{proof}

\begin{proof}[Multiplication of rational numbers is commutative]
  Let \(x,y\in\mathbb{Q}\). Then \(x=a/b\) and \(y=c/d\) for some \(a,b,c,d\in
  \mathbb{Z}\) with \(b\neq0\) and \(d\neq0\). The commutativity of
  multiplication of rational numbers follows from the commutativity of
  multiplication of integers:
  \begin{equation*}
    x\cdot y = \frac{a}{b}\cdot\frac{c}{d} = \frac{a\cdot c}{b\cdot d}
    = \frac{c\cdot a}{d\cdot b} = \frac{c}{d}\cdot\frac{a}{b} = y\cdot x.
  \end{equation*}
  Therefore for all \(x,y\in\mathbb{Q}\), \(x\cdot y = y\cdot x\).
\end{proof}

\((\mathbb{Q}\smallsetminus\{0\},\cdot)\) is a commutative group, but
\((\mathbb{Q},\cdot)\) is a commutative monoid, since zero has no
multiplicative inverse.\index{q@\(\mathbb{Q}\)!group|)}

\vspace{2ex}\index{q@\(\mathbb{Q}\)!ring/field|(}
\textbf{c.}\quad Determine whether the triple \((\mathbb{Q},+,\cdot)\) is a ring
or a field.

\noindent \((\mathbb{Q},+,\cdot)\) is both a ring \emph{and} a field. To prove
that it is a ring, we make use of the proven fact that \((\mathbb{Q},+)\) is a
commutative group.

\begin{proof}[\((\mathbb{Q},+,\cdot)\) is a ring]
  \hfill
  \begin{enumerate}[nolistsep]
    \item \((\mathbb{Q},+)\) is a commutative group, as we've proven in part
    \textbf{a} of this problem.
    \item \(\mathbb{Q}\) is closed with respect to an associative
    multiplication. This follows from the first two proofs in part \textbf{b}.
    \item The left and right distributive laws hold in \(\mathbb{Q}\).
  \end{enumerate}
  \begin{proof}
    Let \(x,y,z\in\mathbb{Q}\). Then \(x=a/b\), \(y=c/d\), and \(z=e/f\) for
    some \(a,b,c,d,e,f\in\mathbb{Z}\) with \(b\neq0\), \(d\neq0\), and
    \(f\neq0\). Then, using the closure of \(\mathbb{Z}\) under addition and
    multiplication and the left distributive law for \(\mathbb{Z}\):
    \begin{align*}
      x\cdot(y+z) &= \frac{a}{b}\cdot\left(\frac{c}{d}+\frac{e}{f}\right)
      = \frac{a}{b}\cdot\frac{cf+de}{df} = \frac{a(cf+de)}{bdf}
      = \frac{acf + ade}{bdf}\\ &= \frac{ac\cancel{f}}{bd\cancel{f}} +
      \frac{a\cancel{d}e}{b\cancel{d}f} = \frac{a\cdot c}{b\cdot d} +
      \frac{a\cdot e}{b\cdot f} = \frac{a}{b}\cdot\frac{c}{d} + \frac{a}{b}
      \cdot\frac{e}{f} = x\cdot y + x\cdot z,
    \end{align*}
    therefore the left distributive law holds in \(\mathbb{Q}\). Now, using
    the closure of \(\mathbb{Z}\) under addition and multiplication and the
    right distributive law for \(\mathbb{Z}\):
    \begin{align*}
      (x+y)\cdot z &= \left(\frac{a}{b} + \frac{c}{d}\right)\cdot\frac{e}{f}
      = \left(\frac{ad+bc}{bd}\right)\cdot\frac{e}{f} = \frac{(ad + bc)e}{bdf}
      = \frac{ade+bce}{bdf}\\ &= \frac{a\cancel{d}e}{b\cancel{d}f}
      + \frac{\cancel{b}ce}{\cancel{b}df} = \frac{a\cdot e}{b\cdot f}
      + \frac{c\cdot e}{d\cdot f} = \frac{a}{b}\cdot\frac{e}{f} +
      \frac{c}{d}\cdot\frac{e}{f} = x\cdot z + y\cdot z,
    \end{align*}
    therefore the right distributive law holds in \(\mathbb{Q}\).
  \end{proof}

  All three conditions of the definition of a ring are satisfied, so
  \((\mathbb{Q},+,\cdot)\) is a ring.
\renewcommand{\qedsymbol}{$\blacksquare$}\end{proof}

\index{field}
\marginnote{Let \(F\) be a ring. Then \(F\) is a \emph{field} iff:
  \begin{enumerate}[nolistsep]
    \item \(F\) is a commutative ring,
    \item \(F\) has a unity \(e\) and \(e\neq0\), and
    \item Every nonzero element of \(F\) has a multiplicative inverse.
  \end{enumerate}}
\begin{proof}[\((\mathbb{Q},+,\cdot)\) is a field]
  The proof that \((\mathbb{Q},+,\cdot)\) is a field has three parts
  corresponding to the three conditions of the definition.
  \begin{proof}[\(\mathbb{Q}\) is a commutative ring]
    The previous proof establishes that \(\mathbb{Q}\) is a ring. For this
    ring to be considered `commutative', multiplication in \(\mathbb{Q}\) must
    be commutative. This is shown by the fifth proof in part \textbf{b} of
    this problem.
  \end{proof}
  \begin{proof}[\(\mathbb{Q}\) has a unity \(e\), and \(e\neq0\)]
    For any ring, the unity \(e\) is an element of the ring such that \(x\cdot
    e = e\cdot x = x\) for all other members \(x\) of the ring.
    In this case, the unity is the multiplicative identity element of
    \(\mathbb{Q}\), which the third proof in part \textbf{b} shows is the
    number one. Here, \(e=1\).
  \end{proof}
  \begin{proof}[Every nonzero\nopunct]\emph{element\footnote{It is interesting
    that `nonzero' is specified here, unlike in the definition of a group.} of
    \(\mathbb{Q}\) has a multiplicative inverse.}
    This is shown by the fourth proof in part \textbf{b}, above.
  \end{proof}
  All three conditions of the definition of a field are satisfied.
  \((\mathbb{Q},+,\cdot)\) is a field.
\renewcommand{\qedsymbol}{$\blacksquare$}\end{proof}
\index{q@\(\mathbb{Q}\)!ring/field|)}
