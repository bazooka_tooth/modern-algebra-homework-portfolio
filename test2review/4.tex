% !TEX root = ../portfolio.tex
\vspace{2ex}
\index{power set!ring|(}
\noindent\textbf{4.}\quad Suppose \(U\) is a nonempty universal set and
\(\mathscr{P}(U)\) is the power set of \(U\). For every element \(u,v\in U\),
define \(\oplus\) and \(\odot\) as\index{set!symmetric difference}\footnote{In
mathematics, \(\oplus\) is known as the `symmetric difference', and in computer
science it is known as `exclusive or'. This operation is similar to the usual
meaning of the word `or' in the English language, representing one or the other,
\emph{but not both}.}
\begin{equation*}
  u\oplus v = (u\cup v)\smallsetminus(u\cap v)\quad\text{and}\quad
  u\odot v = u\cap v.
\end{equation*}
Show that the power set \(\mathscr{P}(U)\) is a ring with respect to the
operations \(\oplus\) and \(\odot\).

\begin{proof}[\((\mathscr{P}(U),\oplus,\odot)\) is a ring]
  The proof consists of eight sub-proofs, corresponding to each of the eight
  conditions of the definition of a ring.\footnote{See the margin of the
  following page for the \hyperlink{ringfull}{full definition}.}
  \begin{proof}[\(\mathscr{P}(U)\) is closed under \(\oplus\)]
    Let \(X,Y\in\mathscr{P}(U)\). Then \(X,Y\subseteq U\), and
    \begin{alignat*}{2}
               && X\oplus Y &= (X\cup Y)\smallsetminus(X\cap Y)\\
      \implies && X\oplus Y &\subseteq X\cup Y\text{ and } X\cup Y \subseteq
      U\\
      \implies && X\oplus Y &\subseteq U\\
      \therefore\quad && X\oplus Y &\in \mathscr{P}(U).
    \end{alignat*}
    Therefore \(X,Y\in\mathscr{P}(U)\implies X\oplus Y\in\mathscr{P}(U)\).
  \end{proof}
  \begin{proof}[\(\oplus\) in \(\mathscr{P}(U)\) is\nopunct]
    \hspace{-0.25em}\emph{\hypertarget{review2.4.associative}{associative.}}
    \footnote{\hyperlink{1.1.40b}{Problem 40 part \textbf{b}} in the first
    chapter of this text was a Venn diagram that demonstrates this
    associativity, and although pictures aren't proof, this exercise is given as
    the justification for associativity in example 5 of section 5.1 of the
    textbook. \hyperlink{3.1.36a.associative}{Problem 36 part \textbf{a}} in the
    fifth chapter of this text contains a proof that uses only tools from
    section 1.1 of the textbook, but the proof is long and difficult to read
    (never mind verify).} This proof follows one given by Majid Hosseini in
    \emph{Mathematics Magazine}\cite{Hosseini2006}, but fills in the gaps in
    understanding that a student of this course might find while reading his
    article.

    First, we need
    to define the \emph{characteristic} function, or \emph{indicator} function.
    The characteristic function \(\chi\) of a set \(X\) takes one argument,
    \(x\), which may or may not be a member of the set \(X\). The value of
    \(\chi_X\) is given by
    \begin{equation*}
      \chi_X(x) =
      \begin{cases}
        1, &\text{if }x\in X,\\
        0, &\text{if }x\notin X,
      \end{cases}
    \end{equation*}
    so that the value of \(\chi_X(x)\) indicates whether \(x\) is a member of
    the set \(X\). In this way, \(\chi\) encodes the contents of a set. Two sets
    are equal only if their contents are the same, so two subsets \(X\) and
    \(Y\) of \(\mathscr{P}(U)\) are equal only if \(\chi_X = \chi_Y\), that is,
    they are equal only if their characteristic functions return the same values
    for the same input.

    \index{ring!definition}
    \marginnote{\begin{defn}\hypertarget{ringfull}{A set} \(R\) in which a
    relation of equality is defined is a \emph{ring} with respect to predefined
    operations of addition, \(+\), and multiplication, \(\cdot\), iff:
    \begin{enumerate}[nolistsep]
      \item \(R\) is \emph{closed} under addition, i.e.\ \(x,y\in R\implies
      x+y\in R\),
      \item Addition in \(R\) is \emph{associative}, i.e.\ \(\forall x,y,z\in R,
      \enspace x+(y+z)=(x+y)+z\),
      \item \(R\) contains an \emph{additive identity}, i.e.\ \(\exists0\in R
      \enspace\forall x\in R\) s.t.\ \(x+0=0+x=x\),
      \item \(R\) contains \emph{additive inverses}, i.e.\ \(\forall x\in R
      \enspace\exists(-x)\in R\) s.t.\ \(x+(-x)=(-x)+x=0\),
      \item Addition in \(R\) is \emph{commutative}, i.e.\ \(\forall x,y\in R,
      \enspace x+y=y+x\),
      \item \(R\) is \emph{closed} under multiplication, i.e.\ \(x,y\in R
      \implies x\cdot y\in R\),
      \item Multiplication in \(R\) is \emph{associative}, i.e.\ \(\forall x,y,z
      \in R,\enspace x\cdot(y\cdot z) = (x\cdot y)\cdot z\), and
      \item Two \emph{distributive laws} hold in \(R\), i.e.\ \(\forall x,y,z\in
      R,\enspace x\cdot(y+z)=x\cdot y+x\cdot z\) and \((x+y)\cdot z = x\cdot z +
      y\cdot z\).
    \end{enumerate}
    See also the \hyperlink{ringalternate}{alternate definition of a ring} in
    problem 1, above.\end{defn}}
    Now we'll prove a statement concerning the characteristic function of a
    symmetric difference. Suppose that \(x\in X\) and \(x\in Y\). Then \(x\notin
    X\oplus Y\), so \(\chi_{X\oplus Y}(x) = 0\), but \(\chi_X(x)=1\) and
    \(\chi_{Y}(x) = 1\), so
    \begin{equation*}
      0 = \chi_{X\oplus Y}(x) = (\chi_X(x) - \chi_Y(x))^2 = (1-1)^2 = 0^2 = 0.
    \end{equation*}
    Suppose that \(x\in X\) and \(x\notin Y\). Then \(x\in X\oplus Y\), so
    \(\chi_{X\oplus Y}(x) = 1\), but \(\chi_X(x)=1\) and \(\chi_{Y}(x) = 0\),
    so
    \begin{equation*}
      1 = \chi_{X\oplus Y}(x) = (\chi_X(x) - \chi_Y(x))^2 = (1-0)^2 = 1^2 = 1.
    \end{equation*}
    Suppose that \(x\notin X\) and \(x\in Y\). Then \(x\in X\oplus Y\), so
    \(\chi_{X\oplus Y}(x) = 1\), but \(\chi_X(x)=0\) and \(\chi_{Y}(x) = 1\).
    Then
    \begin{equation*}
      1 = \chi_{X\oplus Y}(x) = (\chi_X(x) - \chi_Y(x))^2 = (0-1)^2 = (-1)^2
      = 1.
    \end{equation*}
    Suppose that \(x\notin X\) and \(x\notin Y\). Then \(x\notin X\oplus Y\),
    so \(\chi_{X\oplus Y}(x) = 0\), \(\chi_X(x)=0\), and \(\chi_{Y}(x) = 0\).
    Then
    \begin{equation*}
      0 = \chi_{X\oplus Y}(x) = (\chi_X(x) - \chi_Y(x))^2 = (0-0)^2 = 0^2 = 0.
    \end{equation*}
    In any case, \(\chi_{X\oplus Y}(x) = (\chi_X(x) - \chi_Y(x))^2\).

    Now \((\chi_X(x) - \chi_Y(x))^2 = \chi_X^2(x) + \chi_Y^2(x)
    -2\chi_X(x)\chi_Y(x)\), but since the possible values of \(\chi\) are just
    zero and one, \(\chi_S^2 = \chi_S\) for any set \(S\). Therefore
    \begin{equation*}
      \chi_{X\oplus Y}(x) = \chi_X(x) + \chi_Y(x) - 2\chi_X(x)\chi_Y(x).
    \end{equation*}

    Given three subsets \(X\), \(Y\), and \(Z\) of \(\mathscr{P}(U)\), we apply
    this equation to the set \(X\oplus(Y\oplus Z)\), and we find that the
    associativity of symmetric difference follows from the properties of the
    ring \((\mathbb{Z},+,\cdot)\):
    \begin{align*}
      \chi_{X\oplus(Y\oplus Z)} &= \chi_X + \chi_{Y\oplus Z}
      - 2\chi_X\chi_{Y\oplus Z}\\
      &= \chi_X + (\chi_Y + \chi_Z - 2\chi_Y\chi_Z) - 2\chi_X
      (\chi_Y + \chi_Z - 2\chi_Y\chi_Z)\\
      &= \chi_X + \chi_Y + \chi_Z - 2\chi_Y\chi_Z - 2\chi_X\chi_Y
      - 2\chi_X\chi_Z + 4\chi_X\chi_Y\chi_Z\\
      &= \chi_X + \chi_Y - 2\chi_X\chi_Y + \chi_Z - 2\chi_X\chi_Z
      - 2\chi_Y\chi_Z + 4\chi_X\chi_Y\chi_Z\\
      &= (\chi_X + \chi_Y - 2\chi_X\chi_Y) + \chi_Z - 2(\chi_X + \chi_Y
      - 2\chi_X\chi_Y)\chi_Z\\
      &= \chi_{X\oplus Y} + \chi_Z - 2\chi_{X\oplus Y}\chi_Z\\
      &= \chi_{(X\oplus Y)\oplus Z}.
    \end{align*}
  \end{proof}
  \begin{proof}[\(\mathscr{P}(U)\) contains an additive identity]
    Let \(X\in\mathscr{P}(U)\). Then
    \begin{align*}
      X\oplus\varnothing &= (X\cup\varnothing)\smallsetminus(X\cap\varnothing)
      = X\smallsetminus\varnothing = X,\quad\text{and}\\
      \varnothing\oplus X &= (\varnothing\cup X)\smallsetminus(\varnothing\cap
      X) = X\smallsetminus\varnothing = X,
    \end{align*}
    therefore for all \(X\in\mathscr{P}(U)\), \(X\oplus\varnothing =
    \varnothing\oplus X = X\), that is, the empty set is the additive
    identity.
  \end{proof}
  \begin{proof}[\(\mathscr{P}(U)\) contains additive inverses]
    For an arbitrary \(X\in\mathscr{P}(U)\),
    \begin{equation*}
      X\oplus X = (X\cup X)\smallsetminus(X\cap X) = X\smallsetminus X
      = \varnothing,
    \end{equation*}
    that is, the additive identity \(\varnothing\) is the symmetric difference
    of any set \(X\in\mathscr{P}(U)\) with itself, therefore each member of
    \(\mathscr{P}(U)\) is its own additive inverse.
  \end{proof}
  \begin{proof}[\(\oplus\) in \(\mathscr{P}(U)\) is commutative]
    This follows from the commutativity of both union and intersection, which
    follow, in turn, from the commutativity of disjunction and conjunction,
    respectively. Symbolically, let \(X,Y\in\mathscr{P}(U)\). Then
    \begin{align*}
      X\oplus Y &= (X\cup Y)\smallsetminus(X\cap Y)\\
      &= \{x\mid x\in X\lor x\in Y\}\smallsetminus\{x\mid x\in X\land x\in
      Y\}\\
      &= \{x\mid x\in Y\lor x\in X\}\smallsetminus\{x\mid x\in Y\land x\in
      X\}\\
      &= (Y\cup X)\smallsetminus(Y\cap X)\\
      &= Y\oplus X.
    \end{align*}
    Therefore, for all \(X,Y\in\mathscr{P}(U)\), \(X\oplus Y = Y\oplus X\).
  \end{proof}
  \begin{proof}[\(\mathscr{P}(U)\) is closed under \(\odot\)]
    Let \(X,Y\in\mathscr{P}(U)\). Then \(X\subseteq U\). Because the
    intersection of sets is a subset of each, \(X\odot Y = X\cap Y\subseteq X\),
    and because the subset relation is transitive, \(X\oplus Y\subseteq U\). By
    the definition of the power set, \(X\odot Y\in\mathscr{P}(U)\).
  \end{proof}
  \begin{proof}[\(\odot\) in \(\mathscr{P}(U)\) is associative]
    This is equivalent to the statement that intersection is associative, which
    follows from the associativity of conjunction. Symbolically,
    \begin{align*}
      X\odot(Y\odot Z) &= X\cap(Y\cap Z)\\
      &= \{x\mid x\in X\land(x\in Y\land x\in Z)\}\\
      &= \{x\mid(x\in X\land x\in Y)\land x\in Z\}\\
      &= (X\cap Y)\cap Z = (X\odot Y)\odot Z.
    \end{align*}
  \end{proof}
  \begin{proof}[The left and right distributive laws hold in \(\mathscr{P}(U)\)]
    Let \(X,Y,Z\in\mathscr{P}(U)\). For this, we will use an alternative
    expression for set complement,
    \footnote{\label{setdiff}See the beginning of the associativity proof in
    \hyperlink{3.1.36a.associative}{problem 36 part \textbf{a}} in the fifth
    chapter of this text for a proof that \(A\smallsetminus B =
    A\cap\overline{B}\).}
    \(A\smallsetminus B = A\cap\overline{B}\). Then the left distributive law
    holds:
    \begin{alignat*}{2}
      X\odot(Y\oplus Z) &= X\cap(Y\oplus Z)&&\quad\text{by the definition of
      \(\odot\)}\\
      &= X\cap[(Y\cup Z)\smallsetminus(Y\cap Z)]&&\quad\text{by the definition
      of \(\oplus\)}\\
      &= X\cap (Y\cup Z)\cap\overline{Y\cap Z}&&\quad
      \text{see\textsuperscript{\ref{setdiff}}}\\
      &= X\cap(Y\cup Z)\cap(\overline{Y}\cup\overline{Z})&&\quad\text{by
      DeMorgan's law}\\
      &= X\cap(\overline{Y}\cup\overline{Z})\cap(Y\cup Z)&&\quad\text{\(\cap\)
      is commutative}\\
      &= [(X\cap\overline{Y})\cup(X\cap\overline{Z})]\cap(Y\cup Z)&&\quad
      \text{\(\cap\) distributes over \(\cup\)}\\
      &= [(X\cap\overline{X})\cup(X\cap\overline{Y})\\
      &\hphantom{=}\cup(X\cap\overline{Z})]
      \cap(Y\cup Z)&&\quad\text{\(\varnothing\) is the \(\cup\)-identity}\\
      &= X\cap(\overline{X}\cup\overline{Y}\cup\overline{Z})\cap(Y\cup Z)&&
      \quad\text{\(\cap\) distributes over \(\cup\)}\\
      &= X\cap(Y\cup Z)\cap(\overline{X}\cup\overline{Y}\cup\overline{Z})&&
      \quad\text{\(\cap\) is commutative}\\
      &= [(X\cap Y)\cup(X\cap Z)]\cap(\overline{X}\cup\overline{Y}\cup
      \overline{Z})&&\quad\text{\(\cap\) distributes over \(\cup\)}\\
      &= [(X\cap Y)\cup(X\cap Z)]\cap\overline{X\cap Y\cap Z}&&\quad\text{by
      DeMorgan's law}\\
      &= [(X\cap Y)\cup(X\cap Z)]\smallsetminus(X\cap Y\cap Z)&&\quad
      \text{see\textsuperscript{\ref{setdiff}}}\\
      &= [(X\cap Y)\cup(X\cap Z)]\\
      &\hphantom{=}\smallsetminus[(X\cap Y)\cap(X\cap Z)]&&\quad
      \text{since \(X=X\cap X\)}\\
      &= (X\cap Y)\oplus(X\cap Z)&&\quad\text{by the definition of
      \(\oplus\)}\\
      &= X\odot Y\oplus X\odot Z&&\quad\text{by the definition of \(\odot\).}
    \end{alignat*}
    The right distributive law also holds:
    \begin{alignat*}{2}
      (X\oplus Y)\odot Z &= (X\oplus Y)\cap Z&&\quad\text{by the definition of
      \(\odot\)}\\
      &= [(X\cup Y)\smallsetminus(X\cap Y)]\cap Z&&\quad\text{by the
      definition of \(\oplus\)}\\
      &= (X\cup Y)\cap\overline{X\cap Y}\cap Z&&\quad
      \text{see\textsuperscript{\ref{setdiff}}}\\
      &= (X\cup Y)\cap(\overline{X}\cup\overline{Y})\cap Z&&\quad\text{by
      DeMorgan's law}\\
      &= (X\cup Y)\cap[(\overline{X}\cap Z)\cup(\overline{Y}\cap Z)]&&\quad
      \text{\(\cap\) distributes over \(\cup\)}\\
      &= (X\cup Y)\cap[(\overline{X}\cap Z)\\
      &\hphantom{=}\cup(\overline{Y}\cap Z)\cup
      (\overline{Z}\cap Z)]&&\quad\text{\(\varnothing\) is the
      \(\cup\)-identity}\\
      &= (X\cup Y)\cap(\overline{X}\cup\overline{Y}\cup\overline{Z})\cap Z&&
      \quad\text{\(\cap\) distributes over \(\cup\)}\\
      &= (X\cup Y)\cap Z\cap(\overline{X}\cup\overline{Y}\cup\overline{Z})&&
      \quad\text{\(\cap\) is commutative}\\
      &= [(X\cap Z)\cup(Y\cap Z)]\cap(\overline{X}\cup\overline{Y}\cup
      \overline{Z})&&\quad\text{\(\cap\) distributes over \(\cup\)}\\
      &= [(X\cap Z)\cup(Y\cap Z)]\cap\overline{X\cap Y\cap Z}&&\quad\text{by
      DeMorgan's law}\\
      &= [(X\cap Z)\cup(Y\cap Z)]\smallsetminus(X\cap Y\cap Z)&&\quad
      \text{see\textsuperscript{\ref{setdiff}}}\\
      &= [(X\cap Z)\cup(Y\cap Z)]\\
      &\hphantom{=}\smallsetminus[(X\cap Z)\cup(Y\cap Z)]&&
      \quad\text{since \(Z=Z\cap Z\)}\\
      &= (X\cap Z)\oplus(Y\cap Z)&&\quad\text{by the definition of
      \(\oplus\)}\\
      &= X\odot Z\oplus Y\odot Z&&\quad\text{by the definition of \(\odot\).}
    \end{alignat*}
  \end{proof}
All eight conditions required by the definition of a ring are satisfied,
therefore \((\mathscr{P}(U),\oplus,\odot)\) is a ring.
\renewcommand{\qedsymbol}{$\blacksquare$}\end{proof}
\index{power set!ring|)}
