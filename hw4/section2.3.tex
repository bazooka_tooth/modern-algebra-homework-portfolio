% !TEX root = ../portfolio.tex
\index{equivalence relation|(}
\noindent\textbf{18.}\quad Let \(R\) be the relation defined on the set of
integers \(\mathbb{Z}\) by \(aRb\) if and only if \(a\mid b\). Prove or disprove
that \(R\) is an equivalence relation.\marginnote{\begin{defn}
	Let \(a,b\in\mathbb{Z}\). Then \(a\) \emph{divides} \(b\) iff \(\exists c\in
	\mathbb{Z}\) such that \(b=ac\). If \(a\) divides \(b\), we write \(a|b\).
\end{defn}}\index{z@\(\mathbb{Z}\)!divisibility}\footnote{See the
\hyperlink{eqreln}{equivalence relation definition} in the margin at the
beginning of section 1.7 in the second chapter of this text.}

The `divides' relation is not an equivalence relation, since it is not
symmetric.
\begin{proof}
	Suppose that \(R\) were symmetric, that is, assume that \(xRy\) and \(yRx\)
	for all \(x,y\in\mathbb{Z}\). Then by the definition of divisibility there are
	integers \(a\) and \(b\) such that \(y=xa\) and \(x=yb\). Substituting for
	\(x\) in the first equation we have \(y=(yb)a=yba\), which is only satisfied
	if \(a=b=1\), so using the first equation we conclude that \(x\) must equal
	\(y\). Symmetry requires that \(xRy\implies yRx\) for \emph{all}
	\(x,y\in\mathbb{Z}\), but we have shown that the implication is satisfied
	\emph{only} when \(x=y\). So \(R\) is not symmetric, therefore \(R\) is not an
	equivalence relation.
\end{proof}

We note that \(R\) is reflexive: by the definition of divisibility, \(xRx\)
(i.e.\ \(x\mid x\)) if there is an integer, say \(e\), such that \(x=xe\). By
definition \(\mathbb{Z}\) contains the multiplicative identity element
\(e=1\) which satisfies \(x=xe\) for all \(x\in\mathbb{Z}\).
Therefore \(R\) is reflexive.

We also note that \(R\) is transitive: Assume that \(xRy\) and \(yRz\), then by
the definition of divisibility there are integers \(a\) and \(b\) such that
\(y=xa\) and \(z=yb\). Substituting for \(y\) in the second equation we have
\(z=(xa)b=xab\). By the closure of the set of integers under multiplication,
\(ab\in\mathbb{Z}\), so \(x\mid z\) and \(xRz\). Therefore \(R\) is transitive.
\index{equivalence relation|)}

\vspace{2ex}
\index{induction!proof by|(}
\noindent\textbf{43.}\quad Use mathematical induction to prove that the
following statement is true for all positive integers \(n\).
\begin{equation*}
	4\text{ is a factor of }3^{2n}-1
\end{equation*}
\begin{proof}
	Let \(P_n\) be the statement \(4\mid(3^{2n}-1)\). When \(n=1\) the statement
	is \(4\mid(3^{2(1)} - 1)\), which is equivalent to the statement \(4\mid8\),
	which by the definition of divisibility is true since \(8=4\cdot2\).

	Assume that \(P_k\) holds for some positive integer \(k\), that is, assume
	that \(4\mid(3^{2k} - 1)\). Since \(4\mid(3^{2k}-1)\), from the definition of
	divisibility it follows that \(3^{2k}-1= 4x\) for some integer \(x\). Hence
	\(3^{2k}=4x+1\) will serve as an inductive hypothesis. Now
	\begin{alignat*}{3}
		3^{2(k+1)} - 1 &= 3^{2k+2} - 1&&\\
		&= 3^2\cdot3^{2k}-1&&\\
		&= 9\cdot3^{2k}-1&&\\
		&= 9\cdot(4x+1) - 1&&\quad\text{ by the inductive hypothesis}\\
		&= 36x + 9 - 1&&\\
		&= 36x + 8&&\\
		&= 4(9x + 2).
	\end{alignat*}
	Since\footnote{by the closure of \(\mathbb{Z}\) under multiplication and
	addition} \((9x + 2)\in\mathbb{Z}\), it follows that \(4\mid(3^{2(k+1)}-1)\). By
	the Principle of Mathematical Induction, the statement is true for all
	positive integers \(n\).
\end{proof}
\index{induction!proof by|)}
