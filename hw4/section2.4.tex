% !TEX root = ../portfolio.tex
\index{GCD|(}
\noindent\textbf{3.} part \textbf{g.}\quad Find the greatest common divisor
\marginnote{\begin{defn}
	For integers \(a\), \(b\), \(c\), and \(d\), \(d\) is the greatest common
	divisor of \(a\) and \(b\) if
	\begin{enumerate}[nolistsep]
		\item \(d>0\),
		\item \(d\mid a\land d\mid b\), and
		\item \(c\mid a\land c\mid b\implies c\mid d\).
	\end{enumerate}
\end{defn}}
\((414, 693)\) and integers \(m\) and \(n\) such that \((414, 693) = 414m +
693n\).

\index{Euclidean algorithm}Applying the Euclidean algorithm, we have:
\begin{alignat*}{3}
	693 &= (1)(414) + \mathbf{279}&&\quad(q_0 = 1, r_1 = 279)\\
	414 &= (1)(\mathbf{279}) + \mathbf{135}&&\quad(q_1 = 1, r_2 = 135)\\
	\mathbf{279} &= (2)(\mathbf{135}) + \mathcircled{\mathbf{9}}&&\quad(q_2 = 2,
	r_3 = 9)\\
	\mathbf{135} &= (15)(\mathbf{9}) &&\quad(q_3 = 15, r_4 = 0).
\end{alignat*}
\marginnote{Using the method of repeated division presented in class, we can
quickly confirm that the greatest common divisor is \(9\):
\begin{center}
	\begin{tabular}{c|c|c|c|c}
		& \(1\) & \(1\) & \(2\) & \(15\)\\\hline
		\(693\) & \(414\) & \(279\) & \(135\) & \(\mathcircled{\mathbf{9}}\)\\\hline
		\(279\) & \(135\) & \(9\) & \(0\) &
	\end{tabular}
\end{center}}
Solving for the remainders, we have:
\begin{align*}
	\mathbf{279} &= (693)(1) + (414)(-1)\\
	\mathbf{135} &= (414)(1) + (279)(-1)\\
	\mathbf{9} &= (279)(1) + (135)(-2).
\end{align*}
Repeatedly substituting for the remainders in the last equation, we have:
\begin{align*}
	\mathbf{9} &= (\mathbf{279})(1) + [(414)(1) + (\mathbf{279})(-1)](-2)\\
	&= (\mathbf{279})(1) + (414)(-2) + (\mathbf{279})(2)\\
	&= (\mathbf{279})(3) + (414)(-2)\quad\text{after the first substitution}\\
	&= [(693)(1) + (414)(-1)](3) + (414)(-2)\\
	&= (693)(3) + (414)(-3) + (414)(-2)\\
	&= (693)(3) + (414)(-5)\quad\text{after the second substitution}.
\end{align*}
Thus \(m=-5\) and \(n=3\) are integers such that
\begin{equation*}
	(414, 693) = 414m + 693n.
\end{equation*}
\textbf{31.} part \textbf{b.}\quad Find the greatest common divisor of
\(26\), \(52\), and \(60\) and write it in the form \(26x + 52y + 60z\) for
integers \(x\), \(y\), and \(z\).\marginnote{\begin{thm}
	Let \(a,b\in\mathbb{Z}\), not both zero. Then their greatest common divisor,
	\(d\), must exist, and \(d=am+bn\) for some \(m,n\in\mathbb{Z}\), and \(d\) is
	the smallest integer expressible in this way. This is known as
	\emph{B\'ezout's Lemma}, and it can be proved by working the division
	algorithm backwards.
\end{thm}}\index{B\'ezout's Lemma}

The Euclidean algorithm will find the greatest common divisor of two integers,
not three. But suppose a positive integer \(g\) is the greatest common divisor
of all three numbers, \(26\), \(52\), and \(60\). Also, suppose \(d\) is the
greatest common divisor of \(26\) and \(52\), and let \(e\) be the greatest
common divisor of \(d\) and \(60\).

Since \(e\mid d\), \(d\mid26\), and \(d\mid52\), it follows that \(e\mid26\) and
\(e\mid52\), so \(e\) is \emph{one} common divisor of all three integers. Now
\(e\) must be the greatest common divisor, as any common divisor of \(26\),
\(52\), and \(60\), say \(f\), divides \(26\) and \(52\) and so must divide
\(d\). Now \(f\) divides both \(60\) and \(d\), so \(f\) divides \(e\).
Therefore no common divisor of the three integers is greater than \(e\), which
is to say that \(e\) is the greatest common divisor.\footnote{What all this
means is that to find the greatest common divisor of three integers, we may find
the greatest common divisor of any two, and then find the greatest common
divisor of \emph{that} number and the third.}

Here, we begin by noting that the greatest common divisor of \((26, 52)\) is
\(26\), since with \(a=26\) and \(b=52\), the greatest common divisor \(26\)
satisfies the three conditions of the definition: \(26\) is a positive integer,
and \(26\mid a\) and \(26\mid b\). The third condition of the definition is the
statement ``\(c\mid a\) and \(c\mid b\) implies \(c\mid 26\)'', which is
vacuously true as \(a=26\).

If we try to apply the Euclidean algorithm to find the greatest common divisor
of \(26\) and \(52\) then the algorithm terminates early; we find a zero
remainder for \(r_1\) on the first step and have no `last nonzero remainder' to
assign as the greatest common divisor. The textbook does say ``...if we put
\(r_0 = b\)...''; it seems to be intended that \(r_0 = b\). If the algorithm
stopped with \(r_1=0\), the greatest common denominator \(b\) could be expressed
as a linear combination \(b = am + bn\) with \(m=0\) and \(n=1\).

\index{Euclid}
An edited version of Thomas L.\ Heath's translation of Euclid's \emph{Elements}
is available online.\cite{Joyce1996} The Euclidean algorithm is presented in
Book VII, Proposition 2, and it solves this early-termination problem by saying
that if \(b\mid a\), then \(\text{GCD}(a, b) = b\): ``Let \(AB\), \(CD\) be the
two given numbers not prime to one another. Thus it is required to find the
greatest common measure of \(AB\), \(CD\). If now \(CD\) measures \(AB\)--and it
also measures itself--\(CD\) is a common measure of \(CD\), \(AB\). And it is
manifest that it is also the greatest; for no greater number than \(CD\) will
measure \(CD\).''
% Wikipedia's Euclidean algorithm page solves the problem by stating that \(a\)
% and \(b\) should be swapped, if necessary, so that \(a>b\), and begins the
% algorithm with ``the remainders \(r_{-2}\) and \(r_{-1}\) equal \(a\) and
% \(b\)'', so that if the first computed remainder (which they call \(r_0\)) is
% found to be zero, the `last nonzero remainder' will be \(b\).}

Since the greatest common divisor of \(26\) and \(52\) is \(26\), the greatest
common divisor of all three numbers \(26\), \(52\), and \(60\) is the same as
the greatest common divisor of \(26\) and \(60\). We apply the Euclidean
algorithm:
\begin{alignat*}{3}
	60 &= (2)(26) + \mathbf{8}&&\quad(q_0 = 2, r_1 = 8)\\
	26 &= (3)(\mathbf{8}) + \mathcircled{\mathbf{2}}&&\quad(q_1 = 3, r_2 = 2)\\
	\mathbf{8} &= (4)(\mathbf{2})&&\quad(q_2 = 4, r_3 = 0),
\end{alignat*}
and we conclude that the greatest common divisor \((60, 26)\) is \(2\).
Solving for the remainders, we have:
\begin{align*}
	\mathbf{8} &= (60)(1) + (26)(-2)\\
	\mathbf{2} &= (26)(1) + (8)(-3).
\end{align*}
Substituting for the remainder \(\mathbf{8}\) in the last equation, we find:
\begin{align*}
	\mathbf{2} &= (26)(1) + [(60)(1) + (26)(-2)](-3)\\
	&= (26)(1) + (60)(-3) + (26)(6)\\
	&= (26)(7) + (60)(-3).
\end{align*}
We need no help from \(52\); we can express the greatest common divisor \(2\)
as
\begin{equation*}
	\mathbf{2} = 26x + 52y + 60z,
\end{equation*}
with \(x = 7\), \(y = 0\), and \(z = -3\).
\index{GCD|)}

\vspace{2ex}
\index{prime factorization|(}
\noindent\textbf{34.}\quad Use the fact that \(3\) is a prime to prove that
there do not exist nonzero integers \(a\) and \(b\) such that \(a^2=3b^2\).
Explain how this proves that \(\sqrt{3}\) is not a rational number.
\begin{proof}
	We assume that \(a\) and \(b\) are nonzero integers and begin by factoring the
	equation as follows.
	\begin{equation*}
		a\cdot a = 3b\cdot b.
	\end{equation*}
	Applying the unique factorization theorem,\marginnote{\begin{thm}
		Every integer greater than one is prime or can be expressed as a product of
		prime factors, and this factorization is unique up to the order of the
		factors. This is known as the \emph{unique factorization theorem} or the
		\emph{fundamental theorem of arithmetic}.
	\end{thm}}\index{unique factorization}\index{fundamental theorem}we may
	express \(a\) and \(b\) as products of primes,
	\begin{equation*}
		(p_1p_2\cdots p_r)(p_1p_2\cdots p_r) = 3(q_1q_2\cdots q_s)
		(q_1q_2\cdots q_s),
	\end{equation*}
	where \(r\) and \(s\) are integers greater than or equal to one. On the left
	side we have written \(2r\) prime factors; on the right side we have written
	\(2s\) prime factors plus the prime factor three, that is, \(2s + 1\). The
	products on both sides are necessarily equal, and by the uniqueness of this
	factorization the counts of their prime factors are also equal, thus \(2r = 2s +
	1\); we emphasize that \(r\) and \(s\) are integers. Since the left side is an
	even integer and the right side is odd, these quantities cannot be equal. This
	contradicts unique factorizations of \(a\) and \(b\), therefore \(a\) and
	\(b\) cannot both be integers and the equation \(a^2 = 3b^2\) cannot have
	nonzero integer solutions.
\end{proof}

If \(\sqrt{3}\) were rational, it could be expressed as a fully reduced fraction
of integers \(\sqrt{3} = a/b\). Squaring both sides and solving for \(a^2\)
would give
\begin{equation*}
	a^2 = 3b^2,
\end{equation*}
which we have shown is insoluble, therefore \(\sqrt{3}\) is not rational. The
proof could be generalized to show that any equation of the form \(a^2 = pb^2\),
where \(p\) is prime, has no nonzero integer solutions, and therefore
\(\sqrt{p}\) is always irrational.
\index{prime factorization|)}
