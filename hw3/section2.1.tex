% !TEX root = ../portfolio.tex
\noindent\textbf{14.}\quad Prove the following statement concerning the relation
\(<\) on the set \(\mathbb{Z}\) of all integers: `If \(x<y\), then \(x+z<y+z\).'

The following lemma is used in the proof of the one that follows; the latter is
used in the proof of the statement above. These are the same as exercises 1 and
2 from section 2.1.
\begin{lem}
	For all integers \(x\), \(x\cdot 0 = 0\).
\end{lem}
\begin{proof}
	By the definition of the additive identity in \(\mathbb{Z}\), \(0+0=0\). We
	begin by multiplying both sides by an integer \(x\) to obtain
	\(x\cdot(0+0)=x\cdot0\). Now we have \(x\cdot0+x\cdot0 = x\cdot0\) by the left
	distributive law, and \(x\cdot0+x\cdot0=x\cdot0+0\) by the additive identity.

	By the closure of \(\mathbb{Z}\) under multiplication,
	\(x\cdot0\in\mathbb{Z}\), and by the existence of additive inverses,
	\(-(x\cdot0)\in\mathbb{Z}\). Adding \(-(x\cdot0)\) to both sides of our last
	equation yields
	\begin{alignat*}{4}
		&&-(x\cdot0) + (x\cdot0 + x\cdot0) &= -(x\cdot0) + (x\cdot0 + 0)&&\\
		\implies&&(-(x\cdot0) + x\cdot0) + x\cdot0 &= (-(x\cdot0) + x\cdot0) + 0&&
		\enspace\text{by additive associativity}\\
		\implies&&0 + x\cdot0 &= 0 + 0&&\enspace\text{by additive inverses, and}\\
		\implies&&x\cdot0 &= 0&&\enspace\text{by the additive identity.}
	\end{alignat*}
\end{proof}
\begin{lem}
	For all integers \(x\), \(-x = (-1)x\).
\end{lem}
\begin{proof}
	Multiplying both sides of \(-1 = -1\) by an integer \(x\), we find that
	\begin{alignat*}{3}
		(-1)x &= (-1)x&&\\
		&= (-1)x + 0&&\quad\text{by the additive identity,}\\
		&= (-1)x + (x + (-x))&&\quad\text{by additive inverses,}\\
		&= (-1)x + x + (-x)&&\quad\text{by additive associativity,}\\
		&= (-1)x + (1)x + (-x)&&\quad\text{by the multiplicative identity,}\\
		&= x(-1) + x(1) + (-x)&&\quad\text{by multiplicative commutativity,}\\
		&= x(1) + x(-1) + (-x)&&\quad\text{by additive commutativity,}\\
		&= x(1 + (-1)) + (-x)&&\quad\text{by the left distributive law,}\\
		&= x\cdot0 + (-x)&&\quad\text{by additive inverses,}\\
		&= 0 + (-x)&&\quad\text{by Lemma 1, and}\\
		&= -x&&\quad\text{by the additive identity.}
	\end{alignat*}
\end{proof}
Now we are prepared to prove the original statement: `If \(x<y\), then
\(x+z<y+z\).'\marginnote{\begin{defn}
	\hypertarget{orderrelation}{For \(x,y\in\mathbb{Z}\),}
	\begin{equation*}
		x<y\iff y-x\in\mathbb{Z}^+\!,
	\end{equation*}
	where \(y-x=y+(-x)\).
\end{defn}}\index{order relation}
\begin{proof}
	Suppose \(x < y\) for some \(x,y\in\mathbb{Z}\) and let \(z\in\mathbb{Z}\). By
	the definition of the order relation `less than', \(x < y\) if and only if
	\(y - x\in\mathbb{Z}^+\)\!. Now
	\begin{alignat*}{3}
		y - x &= y - x + 0&&\quad\text{by the additive identity,}\\
		&= y - x + (z + (-z))&&\quad\text{by additive inverses,}\\
		&= y - x + z + (-z)&&\quad\text{by additive associativity,}\\
		&= y + (-1)x + z + (-1)z&&\quad\text{by Lemma 2,}\\
		&= y + z + (-1)x + (-1)z&&\quad\text{by additive commutativity,}\\
		&= y + z + (-1)(x + z)&&\quad\text{by the left distributive law,}\\
		&= (y + z) + (-1)(x + z)&&\quad\text{by additive associativity, and}\\
		&= (y + z) - (x + z)&&\quad\text{by Lemma 2.}
	\end{alignat*}
	Therefore \((y + z) - (x + z)\in\mathbb{Z}^+\)\!, so by the definition of
	`less than', \(x + z < y + z\).
\end{proof}

\noindent\textbf{26.}\quad Prove that the cancellation law for multiplication
holds in \(\mathbb{Z}\). That is, if \(xy=xz\) and \(x\neq 0\), then \(y=z\).

Again we will prove two lemmas in support of the statement. These are the same
as exercises 5 and 25 from section 2.1.
\begin{lem}
	For all integers \(x\) and \(y\), \((-x)(-y) = xy\).
\end{lem}
\begin{proof}
	Let \(x,y\in\mathbb{Z}\), then by the closure of \(\mathbb{Z}\) under
	multiplication, \(xy\in\mathbb{Z}\), and by the existence of additive
	inverses there exists an element \(-(xy)\in\mathbb{Z}\) such that
	\(xy + (-(xy)) = 0\). Adding the integer \(-(xy)\) to both sides of \((-x)(-y)
	= xy\), and swapping the left and right sides, produces \(xy + (-(xy)) =
	(-x)(-y) + (-(xy))\).
	% \begin{align*}
	% 	(-x)(-y) + (-(xy)) &= xy + (-(xy)),\text{ switching sides,}\\
	% 	xy + (-(xy)) &= (-x)(-y) + (-(xy)).
	% \end{align*}
	The left side is, by the definition of additive inverses, zero, and for the
	right side,
	\begin{alignat*}{3}
		(-x)(-y) + (-(xy))&=(-x)(-y) + (-x)(y)&&\quad\text{by multiplicative
		associativity,}\\
		&=(-x)(-y + y)&&\quad\text{by the left distributive law,}\\
		&=(-x)(y + (-y))&&\quad\text{by additive commutativity,}\\
		&=(-x)\cdot0&&\quad\text{by additive inverses, and}\\
		&=0&&\quad\text{by Lemma 1.}
	\end{alignat*}
	Both the left side and the right side are zero; from the original statement we
	have deduced the identity \(0=0\), and we conclude that the the original
	statement is true.
\end{proof}
\begin{lem}
	If \(x\) and \(y\) are integers and \(xy=0\), then \(x=0\) or \(y=0\).
\end{lem}
\begin{proof}
	We prove the statement by contrapositive. Assume that \(x\neq0\) and
	\(y\neq0\). Then \(x\in\mathbb{Z}^+\) or \(-x\in\mathbb{Z}^+\)\!, and
	\(y\in\mathbb{Z}^+\) or \(-y\in\mathbb{Z}^+\)\!. We address each of the four
	cases:

	Case 1: \(x\in\mathbb{Z}^+\) and \(y\in\mathbb{Z}^+\)\!. Then by the closure
	of \(\mathbb{Z}^+\) under multiplication, \(xy\in\mathbb{Z}^+\)\!, therefore
	\(xy\neq 0\).

	Case 2: \(x\in\mathbb{Z}^+\) and \(-y\in\mathbb{Z}^+\)\!. Then by the closure
	of \(\mathbb{Z}^+\) under multiplication, \(x(-y)\in\mathbb{Z}^+\)\!. Now
	\begin{alignat*}{3}
		x(-y)&=x((-1)y)&&\quad\text{by Lemma 2,}\\
		&=x(-1)y&&\quad\text{by multiplicative associativity, and}\\
		&=(-1)xy&&\quad\text{by multiplicative commutativity.}
	\end{alignat*}
	Hence \((-1)xy\in\mathbb{Z^+}\)\!, so \((-1)xy\neq0\).
	Multiplying both sides by \(-1\), we find that
	\begin{alignat*}{4}
		&&(-1)(-1)xy&\neq(-1)\cdot0&&\\
		\implies&&(-1)(-1)xy&\neq0&&\quad\text{by Lemma 1,}\\
		\implies&&(1\cdot1)xy&\neq0&&\quad\text{by Lemma 3, and}\\
		\implies&&xy&\neq0&&\quad\text{by additive commutativity.}
	\end{alignat*}

	Case 3: \(-x\in\mathbb{Z}^+\) and \(y\in\mathbb{Z}^+\)\!. Then by the closure
	of \(\mathbb{Z}^+\) under multiplication, \((-x)y\in\mathbb{Z}^+\)\!. Now
	\begin{alignat*}{3}
		(-x)y &= ((-1)x)y&&\quad\text{by Lemma 2, and}\\
		&= (-1)xy&&\quad\text{by multiplicative associativity,}
	\end{alignat*}
	so \((-1)xy\in\mathbb{Z}^+\) and from Case 2 it follows that \(xy\neq0\).

	Case 4: \(-x\in\mathbb{Z}^+\) and \(-y\in\mathbb{Z}^+\)\!. Then by
	the closure of \(\mathbb{Z}^+\) under multiplication,
	\((-x)(-y)\in\mathbb{Z}^+\)\!. By Lemma 3, \((-x)(-y) = xy\), so
	\(xy\in\mathbb{Z}^+\), therefore \(xy\neq0\).

	In all cases, \(xy\neq 0\), which is proof of the contrapositive.
\end{proof}

With the help of Lemmas 3 and 4, we prove the original statement: `If \(xy=xz\)
and \(x\neq 0\), then \(y=z\).'
\begin{proof}
	Assume that \(xy=xz\) for some integers \(x\), \(y\), and \(z\), and assume
	that \(x\neq0\). By the closure of the integers under multiplication,
	\(xz\in\mathbb{Z}\), so there exists an additive inverse
	\(-(xz)\in\mathbb{Z}\). Adding this to both sides of the assumed equality
	gives
	\begin{alignat*}{4}
		&&xy + (-(xz)) &= xz + (-(xz))&&\\
		\implies&&xy + (-(xz)) &= 0&&\quad\text{by additive inverses,}\\
		\implies&&xy + (-1)xz &= 0&&\quad\text{by Lemma 2,}\\
		\implies&&xy + x(-1)z &= 0&&\quad\text{by by multiplicative
		commutativity,}\\
		\implies&&x\cdot(y + (-1)z) &= 0&&\quad\text{by the left distributive law,
		and}\\
		\implies&&x\cdot(y + (-z)) &= 0&&\quad\text{by Lemma 2.}
	\end{alignat*}
	By the closure of \(\mathbb{Z}\) under addition and multiplication, both
	factors on the left hand side are integers, so by Lemma 4, either \(x=0\) or
	\(y + (-z) = 0\). But by the hypothesis \(x\neq0\), so it must be the case
	that \(y + (-z) = 0\). Adding \(z\) to both sides of the equation
	\(y + (-z) = 0\) gives
	\begin{alignat*}{4}
		&&y + (-z) + z &= 0 + z&&\\
		\implies&&y + 0 &= 0 + z&&\quad\text{by additive inverses, and}\\
		\implies&&y &= z&&\quad\text{by the additive identity.}
	\end{alignat*}
	Therefore if \(xy=xz\) and \(x\neq 0\), then \(y=z\).
\end{proof}
