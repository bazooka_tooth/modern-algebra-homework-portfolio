% !TEX root = ../portfolio.tex
\noindent\textbf{10.}\quad Prove that the following statement is true for every
positive integer \(n\).\index{induction!proof by}
\begin{equation*}
	\frac{1}{1\cdot2} + \frac{1}{2\cdot3} + \frac{1}{3\cdot4} + \cdots +
	\frac{1}{n(n + 1)} = \frac{n}{n + 1}
\end{equation*}
\begin{proof}
	Let \(P_n\) be the statement above. The statement \(P_1\) is \(1/(1\cdot 2) =
	1/(1 + 1)\), which is clearly true. Assume the statement \(P_k\) is true for
	some arbitrary positive integer \(k\geq 1\), that is, assume
	\begin{equation*}
		\frac{1}{1\cdot2} + \frac{1}{2\cdot3} + \frac{1}{3\cdot4} + \cdots +
		\frac{1}{k(k + 1)} = \frac{k}{k + 1}.
	\end{equation*}
	To prove the original statement \(P_n\), we must show that \(P_k\) implies
	\(P_{k + 1}\), that is
	\begin{equation*}
		\frac{1}{1\cdot2} + \frac{1}{2\cdot3} + \frac{1}{3\cdot4} + \cdots +
		\frac{1}{(k + 1)[(k + 1) + 1]} = \frac{k + 1}{(k + 1) + 1}.
	\end{equation*}
	The first \(k\) terms in the left-hand side of \(P_{k + 1}\) are the same as
	the left-hand side of \(P_k\), so we may replace these by the right-hand side
	of \(P_k\), so for the left-hand side of \(P_{k + 1}\) we obtain
	\begin{multline*}
		\frac{k}{k + 1} + \frac{1}{(k + 1)[(k + 1) + 1]} =
		\frac{k}{k + 1} + \frac{1}{(k + 1)(k + 2)}\\
		= \frac{k(k + 2)}{(k + 1)(k + 2)} + \frac{1}{(k + 1)(k + 2)}
		= \frac{k(k + 2) + 1}{(k + 1)(k + 2)}\\
		= \frac{k^2 + 2k + 1}{(k + 1)(k + 2)}
		= \frac{\cancel{(k + 1)}(k + 1)}{\cancel{(k + 1)}(k + 2)}
		= \frac{k + 1}{(k + 1) + 1}.
	\end{multline*}
	This is the right-hand side of the statement \(P_{k + 1}\). We have shown
	that \(P_1\) is true and that \(P_k\implies P_{k + 1}\), therefore \(P_n\)
	is true for all positive integers \(n\).
\end{proof}

\noindent\textbf{29.}\quad Use mathematical induction to prove that the
following statement is true for all positive integers \(n\).
\index{induction!proof by}
\begin{equation*}
	n < 2^n
\end{equation*}
\begin{proof}
	The statement is true for \(n=1\) since \(1<2^1\). Assume the statment is true
	for an arbitrary \(k\geq 1\), that is, assume \(k < 2^k\).\marginnote{Note
	that multiplying both sides of the inductive hypothesis by two yields
	\(2k < 2\cdot2^k\).} When \(n=k+1\), the left side of the statement is
	\(k+1\), and
	\begin{alignat*}{3}
		k + 1&\leq k + k&&\quad\text{since }k\geq 1\\
		&=2k&&\\
		&<2\cdot2^k&&\quad\text{by the inductive hypothesis}\\
		&=2^{k+1}.&&
	\end{alignat*}
	Since \(2^{k+1}\) is the right side of the inequality when \(n=k+1\), we have
	proved that \(n < 2^n\) is true when \(n=k+1\). Therefore the inequality is
	true for all positive integers \(n\).
\end{proof}

\index{power set!number of elements|(}
\hypertarget{2.2.36}{\noindent\textbf{36.}\quad} Exercises 33-35 can be
generalized as follows: If \(0\leq k\leq n\) and the set \(A\) has \(n\)
elements, then the number of elements of the power set \(\mathscr{P}(A)\)
containing exactly \(k\) elements is \(\binom{n}{k}\).\marginnote{See also
\hyperlink{1.1.10b}{exercise 10 part \textbf{b} of section 1.1.}}

\textbf{a.}\quad Use this result to write an expression for the total number of
elements in the power set \(\mathscr{P}(A)\).

The total number of elements in \(\mathscr{P}(A)\) is the number of elements of
the power set which contain any possible number of elements, that is, the sum of
\(\binom{n}{k}\) from \(k=0\) to \(k=n\),
\begin{equation*}
	\sum_{k=0}^n\binom{n}{k}.
\end{equation*}

\textbf{b.}\quad Use the binomial theorem
\marginnote{\begin{thm}
	The \emph{binomial theorem}, or \emph{binomial identity}, is
	\begin{equation*}
		(a+b)^n = \sum_{r=0}^n\binom{n}{r}a^{n-r}b^r.
	\end{equation*}
\end{thm}}\index{binomial identity}
as stated in exercise 22 to evaluate
the expression in part \textbf{a} and compare this result to exercise 33.
(\emph{Hint:} Set \(a=b=1\) in the binomial theorem.)

Setting \(a=b=1\) in the binomial theorem gives
\begin{equation*}
	(1+1)^n = \sum_{r=0}^n\binom{n}{r}(1)^{n-r}(1)^r.
\end{equation*}
Regardless of \(n\) and \(r\), the terms \((1)^{n-r}\) and \((1)^r\) on the
right side equal one, so their product is one, and the equation above is
equivalent to
\begin{equation*}
	(1 + 1)^n = \sum_{r=0}^n\binom{n}{r}.
\end{equation*}
The index symbol \(r\) is arbitrary, so the right side above is equal to the sum
in part \textbf{a}, therefore
\begin{equation*}
	\sum_{k=0}^n\binom{n}{k} = (1+1)^n = 2^n.
\end{equation*}
Exercise 33 asks for a proof that the power set \(\mathscr{P}(A)\) has \(2^n\)
elements. We proceed by induction.
\begin{proof}
	\marginnote{This proof relies on an argument similar to the solution of
	\hyperlink{1.1.10a}{exercise 10 part \textbf{a} of section 1.1}, but presents
	more explicit inductive reasoning.}
	Suppose an arbitrary set \(A\) has \(n=0\) elements. Then there is only one
	possible subset of \(A\), the empty set, so \(\mathscr{P}(A)\) has one
	element. Since \(2^0 = 1\), the statement holds for the basis case.

	Suppose that the statement holds for some \(n=k\) where \(k\geq 0\), that is,
	assume that for a set \(A\) containing \(k\) elements, the power set
	\(\mathscr{P}(A)\) has \(2^k\) elements. Choose an element \(x\) not in \(A\),
	\(x\in A'\)\!. Let \(B = A\cup\{x\}\). Then since \(A\) contains \(k\)
	elements, \(B\) contains \(k + 1\) elements. For each original element of
	\(\mathscr{P}(A)\), the new element \(x\) in \(B\) presents two new possible
	subsets of \(B\): one where \(x\) is not a member, and one where \(x\) is a
	member. Therefore \(|\mathscr{P}(B)| = 2\cdot|\mathscr{P}(A)|\). By the
	inductive hypothesis \(|\mathscr{P}(A)| = 2^k\), so \(|\mathscr{P}(B)| =
	2\cdot2^k = 2^{k + 1}\).

	We have shown that the statement \(|A| = n\implies|\mathscr{P}(A)| = 2^n\) is
	true when \(n=0\), and that if the statement is true for \(n=k\) then it also
	holds for \(n=k+1\), so by the principle of mathematical induction the
	statement \(|A| = n\implies|\mathscr{P}(A)| = 2^n\) is true for all \(n\geq
	0\).
\end{proof}
Now since \(|A| = n\implies|\mathscr{P}(A)| = 2^n\) and
\(\sum_{k=0}^n\binom{n}{k} = 2^n\), \(|A| = n\implies|\mathscr{P}(A)|
= \sum_{k=0}^n\binom{n}{k}\), that is,
\begin{equation*}
	\sum_{k=0}^n\binom{n}{k}\quad\text{and}\quad 2^n
\end{equation*}
are equivalent.
\index{power set!number of elements|)}
