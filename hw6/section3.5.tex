% !TEX root = ../portfolio.tex
\vspace{2ex}
\noindent\textbf{2.}\quad Let \(G_1\), \(G_2\), and \(G_3\) be groups.

\textbf{a.}\quad Prove that if \(\phi_1\) is an isomorphism from \(G_1\) to
\(G_2\) and \(\phi_2\) is an isomorphism from \(G_2\) to \(G_3\), then
\(\phi_2\phi_1\) is an isomorphism from \(G_1\) to \(G_3\).
\marginnote{\begin{pstrat}
  For \(\phi_2\phi_1\) to be an isomorphism from \(G_1\) to \(G_3\),
  \(\phi_2\phi_1\) must be a one-to-one correspondence from \(G_1\) to \(G_3\),
  and \(\phi_2\phi_1(x\cdot y) = \phi_2\phi_1(x)\cdot\phi_2\phi_1(y)\) for all
  \(x\) and \(y\) in \(G_1\).
\end{pstrat}}
\begin{proof}[\(G_1\cong G_2\land G_2\cong G_3\implies G_1\cong G_3\)]
  Suppose that \(\phi_1\) is an isomorphism from \(G_1\) to \(G_2\) and
  \(\phi_2\) is an isomorphism from \(G_2\) to \(G_3\). Furthermore, let
  \(\circledast\) denote the binary operation associated with \(G_1\), let
  \(\circledcirc\) denote the binary operation associated with \(G_2\), and let
  \(\circleddash\) denote the binary operation associated with \(G_3\). This
  proof consists of the following two parts.
  \begin{proof}[\(\phi_2\phi_1\) is a one-to-one correspondence from \(G_1\)
  to \(G_3\)]
    The mappings \(\phi_1\) and \(\phi_2\) are both one-to-one correspondences
    between their respective groups, so \(\phi_1\) and \(\phi_2\) are both onto
    and both one-to-one.

    \marginnote{\begin{pstrat}
      To show that \(\phi_2\phi_1\) maps \(G_1\) onto \(G_3\), we choose an
      arbitrary \(c\) in \(G_3\) and show that there exists an element \(a\in
      G_1\) such that \(b=(\phi_2\circ\phi_1)(a)\).
    \end{pstrat}}
    Since \(\phi_2\) is onto, for every \(z\in G_3\) there exists a \(y\in G_2\)
    such that \(z=\phi_2(y)\). Since \(\phi_1\) is onto, for every element of
    \(G_2\), in particular, \(y\), there exists an \(x\in G_1\) such that
    \(y=\phi_1(x)\). Now
    \begin{equation*}
      \phi_2\phi_1(x) = (\phi_2\circ\phi_1)(x) = \phi_2(\phi_1(x)) = \phi_2(y)
      = z,
    \end{equation*}
    hence for every \(z\in G_3\), there exists an \(x\in G_1\) such that
    \(z=\phi_2\phi_1(x)\), therefore \(\phi_2\phi_1\) is onto.

    \marginnote{\begin{pstrat}
      To prove that \(\phi_2\phi_1\) is one-to-one, we show that
      \begin{equation*}
        \phi_2\phi_1(x) = \phi_2\phi_1(y)\implies x=y.
      \end{equation*}
    \end{pstrat}}
    Let \(x,y\in G_1\) and suppose that \(\phi_2\phi_1(x) = \phi_2\phi_1(y)\).
    Then
    \begin{alignat*}{3}
      && \phi_2\phi_1(x) &= \phi_2\phi_1(y) &&\\
      \implies && \phi_2(\phi_1(x)) &= \phi_2(\phi_1(y)) &&\quad\text{by
      definition of composition}\\
      \implies && \phi_1(x) &= \phi_1(y) &&\quad\text{since \(\phi_2\) is
      one-to-one}\\
      \implies && x &= y &&\quad\text{since \(\phi_1\) is one-to-one,}
    \end{alignat*}
    hence (by contrapositive) different elements of \(G_1\) always have
    different images under \(\phi_2\phi_1\), therefore \(\phi_2\phi_1\) is
    one-to-one.

    Since \(\phi_2\phi_1\) is both onto and one-to-one, it is a one-to-one
    correspondence from \(G_1\) to \(G_3\).
  \end{proof}
  \begin{proof}[\(\phi_2\phi_1(x\circledast y) = \phi_2\phi_1(x)\circleddash
    \phi_2\phi_1(y)\) \(\forall x,y\in G_1\)]
    Let \(x,y\in G_1\). Then
    \begin{alignat*}{2}
      \phi_2\phi_1(x\circledast y) &= \phi_2(\phi_1(x\circledast y)) &&\quad
      \text{by definition of composition}\\
      &= \phi_2(\phi_1(x)\circledcirc\phi_1(y)) &&\quad\text{since \(\phi_1\)
      is an isomorphism}\\
      &= \phi_2(\phi_1(x))\circleddash\phi_2(\phi_1(y)) &&\quad\text{since
      \(\phi_2\) is an isomorphism}\\
      &= \phi_2\phi_1(x)\circleddash\phi_2\phi_1(y) &&\quad\text{by definition
      of composition.}
    \end{alignat*}
    This holds for an arbitrary \(x,y\in G_1\), therefore it holds for all
    \(x,y\in G_1\).
  \end{proof}
  Since \(\phi_2\phi_1\) is a one-to-one correspondence from \(G_1\) to \(G_3\)
  and the composition \(\phi_2\phi_1\) preserves the group operations between
  \(G_1\) and \(G_3\), it follows that \(\phi_2\phi_1\) is an isomorphism from
  \(G_1\) to \(G_3\).
\renewcommand{\qedsymbol}{$\blacksquare$}\end{proof}

\textbf{b.}\quad If \(\phi_1\) is an isomorphism from \(G_1\) to \(G_3\) and
\(\phi_2\) is an isomorphism from \(G_2\) to \(G_3\), find an isomorphism from
\(G_1\) to \(G_2\).

We will show that \(\phi_1\phi_2^{-1}\) is an isomorphism from \(G_1\) to
\(G_2\) by first proving that \(\phi_2^{-1}\) is an isomorphism from \(G_3\) to
\(G_2\), then by applying the proven statement from part \textbf{a}.

\begin{proof}[\(\phi\colon G\to G'\) is an isomorphism \(\implies\)
  \(\phi^{-1}\colon G'\to G\) is an isomorphism]
  \marginnote{This proof is a solution for the first exercise in section 3.5,
  which was not assigned but helps justify the solution to this part of the
  exercise.}
  Suppose \(\phi\) is an isomorphism from a group \(G\) under an operation
  \(\circledast\) to a group \(G'\) under an operation \(\circledcirc\). First,
  we will prove that an inverse mapping \(\phi^{-1}\) exists.

  Since \(\phi\) is an isomorphism it must be onto and one-to-one. Let \(y\in
  G'\). Since \(\phi\) is onto, there exists an \(x\in G\) such that
  \(y=\phi(x)\). Since \(\phi\) is one-to-one, this choice of \(y\) is unique.
  This enables the definition of a function \(\phi^{-1}\colon G'\to G\), as
  \begin{equation*}
    \phi^{-1}(y) = \text{the unique element } x\in G \text{ such that } y =
    \phi(x).
  \end{equation*}
  By this definition
  \begin{equation*}
    \phi^{-1}(\phi(x)) = \phi^{-1}(y) = x\text{ and }\phi(\phi^{-1}(y)) =
    \phi(x) = y,
  \end{equation*}
  so \(\phi^{-1}\) is the inverse of \(\phi\).

  Next, we will prove that \(\phi^{-1}\) is a one-to-one correspondence. Let
  \(x\in G\). Since \(\phi\) is a one-to-one correspondence, there exists a
  unique \(y\in G'\) such that \(y=\phi(x)\). By the above definition of
  \(\phi^{-1}\), \(x=\phi^{-1}(y)\), so every element of \(G\) is the image
  under \(\phi^{-1}\) of at least one element of \(G'\), therefore \(\phi^{-1}\)
  is onto.

  Let \(y_1,y_2\in G'\), and suppose that \(\phi^{-1}(y_1) = \phi^{-1}(y_2)\).
  Let \(x\in G\) and \(y\in G'\) such that \(x=\phi^{-1}(y_1)\) and
  \(y=\phi(x)\). Now
  \begin{align*}
    y_1 &= \phi(\phi^{-1}(y_1)) = \phi(x) = y,\text{ and}\\
    y_2 &= \phi(\phi^{-1}(y_2)) = \phi(\phi^{-1}(y_1)) = \phi(x) = y,
  \end{align*}
  therefore \(y_1=y_2\) and \(\phi^{-1}\) is one-to-one.

  The mapping \(\phi^{-1}\) is onto and one-to-one, therefore it is a one-to-one
  correspondence. Finally, we will show that \(\phi^{-1}\) respects the group
  operations.

  Let \(y_1,y_2\in G'\), let \(x_1 = \phi^{-1}(y_1)\), and let \(x_2 =
  \phi^{-1}(y_2)\). Then since \(\phi^{-1}\) is the inverse of \(\phi\), \(y_1 =
  \phi(x_1)\) and \(y_2 = \phi(x_2)\). Now
  \begin{alignat*}{2}
    \phi^{-1}(y_1\circledcirc y_2) &= \phi^{-1}(\phi(x_1)\circledcirc
    \phi(x_2))&&\quad\text{given above}\\
    &= \phi^{-1}(\phi(x_1\circledast x_2)) &&\quad\text{since \(\phi\) is an
    isomorphism}\\
    &= x_1 \circledast x_2 &&\quad\text{since \(\phi^{-1}\circ\phi = I_G\)}\\
    &= \phi^{-1}(y_1) \circledast \phi^{-1}(y_2)&&\quad\text{given above},
  \end{alignat*}
  hence for all \(y_1,y_2\in G'\), \(\phi^{-1}(y_1\circledcirc y_2) =
  \phi^{-1}(y_1) \circledast \phi^{-1}(y_2)\).

  We have shown that if \(\phi\colon (G, \circledast)\to (G', \circledcirc)\) is
  an isomorphism, then the mapping \(\phi^{-1}\colon (G', \circledcirc)\to (G,
  \circledast)\) is a one-to-one correspondence that preserves the group
  operations, therefore \(\phi^{-1}\) is an isomorphism.
\end{proof}
Equipped with this fact, we can answer the exercise. Given an isomorphism
\(\phi_2\colon G_2\to G_3\), by the last proof there exists an isomorphism
\(\phi_2^{-1}\colon G_3\to G_2\). Given an isomorphism \(\phi_1\colon G_1\to
G_3\), by the proof in part \textbf{a}, the mapping \(\phi_1\phi_2^{-1}\) is an
isomorphism from \(G_1\) to \(G_2\).

\vspace{2ex}
\index{congruence modulo}
\noindent\textbf{4.}\quad \hypertarget{3.5.4}{Let} \(G=\{1, i, -1, -i\}\) under
multiplication, and let \(G' = \mathbb{Z}_4 = \{[0], [1], [2], [3]\}\) under
addition. Find an isomorphism from \(G\) to \(G'\) that is different from the
one given in Example 3 of this section (in the textbook).

Let \(\phi\colon G\to G'\) be defined by
\begin{equation*}
  \phi(z) = \frac{\arg(z)}{\frac{\pi}{2}},
\end{equation*}
that is, given \(z\in G\), the image of \(z\) under \(\phi\) is the set of
values of the complex argument\index{c@\(\mathbb{C}\)!argument} of \(z\), where
each value is divided by \(\pi/2\).\marginnote{The complex argument of \(z\),
\(\arg(z)\), is the angle \(\theta\) such that \(z = a + ib = r(\cos\theta +
i\sin\theta) = re^{i\theta}\), where \(r = \sqrt{a^2 + b^2}\). For this
exercise, all elements in \(G\) have a radius \(r=1\). See the exercises in the
seventh assignment, in \hyperlink{7}{the eighth chapter} of this text, for more
about complex numbers.} The complex argument function is a set-valued function,
as there are infinite choices for the angle \(\theta\) that will satisfy this
equation for a particular \(z\), due to the periodicity of the \(\sin\) and
\(\cos\) functions. This is exactly what we need; the image of \(\phi\) must be
\(G'\), the set of congruence classes modulo \(4\). Hence \(\phi\) is a
well-defined function from \(G\) to \(G'\).

\begin{margintable}
  \centering
  \begin{tabular}{c|c|c|c}
    \(z\) & \(\Arg(z)\) & \(\arg(z)\) & \(\phi(z)\)\\
    \hline
    \(1\) & \(0\) & \(\{0 + 2\pi n\}\) & \(\{0 + 4n\} = [0]\)\\
    \(i\) & \(\frac{\pi}{2}\) & \(\{\frac{\pi}{2} + 2\pi n\}\) & \(\{1 + 4n\}
    = [1]\)\\
    \(-1\) & \(\pi\) & \(\{\pi + 2\pi n\}\) & \(\{2 + 4n\} = [2]\)\\
    \(-i\) & \(\frac{3\pi}{2}\) & \(\{\frac{3\pi}{2} + 2\pi n\}\) & \(\{3 + 4n\}
    = [3]\)
  \end{tabular}
\end{margintable}
For each possible value \(z\in G\), the table in the margin shows the principal
complex argument value \(\Arg(z)\), the set of complex argument values
\(\arg(z)\), and the value of \(\phi(z)\). The values of \(\phi\) are of the
form \(\{y+4n \mid y\in\{0,1,2,3\}\text{ and }n\in\mathbb{Z}\}\), and in
\(\mathbb{Z}_4\), \([y+4n] = [y] + [4n] = [y]\), as \(4n\equiv 0\pmod{4}\).

That \(\phi\) is a one-to-one correspondence is clear from the table, but we
should check that this function preserves the group operations. Applying Euler's
formula\index{c@\(\mathbb{C}\)!Euler's formula},\marginnote{\begin{thm}
  \emph{Euler's formula} \hypertarget{euler}{in} complex numbers states that for
  any complex number \(z = a+ib\),
  \begin{equation*}
    z = a+ib = r(\cos\theta + i\sin\theta) = re^{i\theta},
  \end{equation*}
  where \(r = \sqrt{a^2 + b^2}\) and \(\theta = \arg z\).
\end{thm}} \(x,y\in G\) and their product \(xy\) can be expressed
as
\begin{alignat*}{4}
  x &= e^{i\theta_1},& y &= e^{i\theta_2},&&\enspace\text{and}\enspace& xy &=
  e^{i(\theta_1 + \theta_2)},\quad\text{so}\\
  \theta_1 &= \frac{\ln x}{i},&\theta_2 &= \frac{\ln y}{i},&
  &\enspace\text{and}\enspace&\theta_1 + \theta_2 &= \frac{\ln(xy)}{i},\\
  \intertext{finally since \(\phi(z) = (\ln z/i)/(\pi/2)\),}
  \phi(x) &= \frac{\theta_1}{\frac{\pi}{2}},&\phi(y) &= \frac{\theta_2}{
  \frac{\pi}{2}},&&\enspace\text{and}\enspace&\phi(xy) &= \frac{\theta_1 +
  \theta_2}{\frac{\pi}{2}}.
\end{alignat*}
% It is a basic fact that \(\ln(xy) = \ln x + \ln y\), and
% \begin{alignat*}{3}
%   &&\ln(xy) &= \ln x + \ln y &&\\
%   \implies i(\theta_1 + \theta_2)
%   \implies && \theta_1 + \theta_2 &= &&\quad\text{after division by \(i\)}\\
%   \implies && \phi(xy) &= \phi(x) + \phi(y) &&\quad\text{after division by
%   \(\pi/2\),}
% \end{alignat*}
Adding\marginnote{Here \(\ln\) represents the complex logarithm, which is another
set-valued function.} the first two fractions above, it is clear that \(\phi(xy) = \phi(x) +
\phi(y)\), therefore \(\phi\) respects the group operations, and since it is
also a one-to-one correspondence, \(\phi\) is an isomorphism from \(G\) to
\(G'\).

\vspace{2ex}
\noindent\textbf{10.}\quad \hypertarget{3.5.10}{Find} an isomorphism from the
multiplicative group
\begin{equation*}
  H = \left\{\begin{bmatrix}1 & 0\\0 & 1\end{bmatrix},\begin{bmatrix}1 & 0\\
  0 & -1\end{bmatrix},\begin{bmatrix}-1 & 0\\0 & 1\end{bmatrix},
  \begin{bmatrix}-1 & 0\\0 & -1\end{bmatrix}\right\}
\end{equation*}
to the group \(G = \{e, a, b, ab\}\) with the multiplication table in the
margin.
\begin{margintable}
  \centering
  \begin{tabular}{c|cccc}
    \(\cdot\) & \(e\) & \(a\) & \(b\) & \(ab\)\\
    \hline
    \(e\) & \(e\) & \(a\) & \(b\) & \(ab\)\\
    \(a\) & \(a\) & \(e\) & \(ab\) & \(b\)\\
    \(b\) & \(b\) & \(ab\) & \(e\) & \(a\)\\
    \(ab\) & \(ab\) & \(b\) & \(a\) & \(e\)
  \end{tabular}
\end{margintable}

Define a mapping \(\phi\colon H\to G\) by
\begin{equation*}
  \phi\left(\begin{bmatrix}1&0\\0&1\end{bmatrix}\right) = e,\enspace\phi\left(
  \begin{bmatrix}1&0\\0&-1\end{bmatrix}\right) = a,\enspace\phi\left(
  \begin{bmatrix}-1&0\\0&1\end{bmatrix}\right) = b,\enspace\text{and}\enspace
  \phi\left(\begin{bmatrix}-1&0\\0&-1\end{bmatrix}\right) = ab.
\end{equation*}
This mapping is clearly a one-to-one correspondence from \(H\) to \(G\). The
multiplication table for \(H\) follows.
\begin{margintable}
  \centering
  \begin{tabular}{c|cccc}
    \(\phi(xy)\) & \(e\) & \(a\) & \(b\) & \(ab\)\\
    \hline
    \(e\) & \(e\) & \(a\) & \(b\) & \(ab\)\\
    \(a\) & \(a\) & \(e\) & \(ab\) & \(b\)\\
    \(b\) & \(b\) & \(ab\) & \(e\) & \(a\)\\
    \(ab\) & \(ab\) & \(b\) & \(a\) & \(e\)
  \end{tabular}
\end{margintable}
\begin{center}
  \begin{tabular}{c|cccc}
    &&&&\\[-1em]
    \(\cdot\) &
    \(\begin{bmatrix}1 & 0\\0 & 1\end{bmatrix}\) &      % e
    \(\begin{bmatrix}1 & 0\\0 & -1\end{bmatrix}\) &     % a
    \(\begin{bmatrix}-1 & 0\\0 & 1\end{bmatrix}\) &     % b
    \(\begin{bmatrix}-1 & 0\\0 & -1\end{bmatrix}\) \\   % ab
    &&&&\\[-1em]
    \hline
    &&&&\\[-1em]
    \(\begin{bmatrix}1 & 0\\0 & 1\end{bmatrix}\) &      % e
    \(\begin{bmatrix}1 & 0\\0 & 1\end{bmatrix}\) &      % e
    \(\begin{bmatrix}1 & 0\\0 & -1\end{bmatrix}\) &     % a
    \(\begin{bmatrix}-1 & 0\\0 & 1\end{bmatrix}\) &     % b
    \(\begin{bmatrix}-1 & 0\\0 & -1\end{bmatrix}\) \\   % ab
    &&&&\\[-1em]
    \(\begin{bmatrix}1 & 0\\0 & -1\end{bmatrix}\) &     % a
    \(\begin{bmatrix}1 & 0\\0 & -1\end{bmatrix}\) &     % a
    \(\begin{bmatrix}1 & 0\\0 & 1\end{bmatrix}\) &      % e
    \(\begin{bmatrix}-1 & 0\\0 & -1\end{bmatrix}\) &    % ab
    \(\begin{bmatrix}-1 & 0\\0 & 1\end{bmatrix}\) \\    % b
    &&&&\\[-1em]
    \(\begin{bmatrix}-1 & 0\\0 & 1\end{bmatrix}\) &     % b
    \(\begin{bmatrix}-1 & 0\\0 & 1\end{bmatrix}\) &     % b
    \(\begin{bmatrix}-1 & 0\\0 & -1\end{bmatrix}\) &    % ab
    \(\begin{bmatrix}1 & 0\\0 & 1\end{bmatrix}\) &      % e
    \(\begin{bmatrix}1 & 0\\0 & -1\end{bmatrix}\) \\    % a
    &&&&\\[-1em]
    \(\begin{bmatrix}-1 & 0\\0 & -1\end{bmatrix}\) &    % ab
    \(\begin{bmatrix}-1 & 0\\0 & -1\end{bmatrix}\) &    % ab
    \(\begin{bmatrix}-1 & 0\\0 & 1\end{bmatrix}\) &     % b
    \(\begin{bmatrix}1 & 0\\0 & -1\end{bmatrix}\) &     % a
    \(\begin{bmatrix}1 & 0\\0 & 1\end{bmatrix}\)        % e
    % \\[-1em]
  \end{tabular}
\end{center}
\begin{margintable}
  \centering
  \begin{tabular}{c|cccc}
    \(\phi(x)\cdot\phi(y)\) & \(e\) & \(a\) & \(b\) & \(ab\)\\
    \hline
    \(e\) & \(e\) & \(a\) & \(b\) & \(ab\)\\
    \(a\) & \(a\) & \(e\) & \(ab\) & \(b\)\\
    \(b\) & \(b\) & \(ab\) & \(e\) & \(a\)\\
    \(ab\) & \(ab\) & \(b\) & \(a\) & \(e\)
  \end{tabular}
\end{margintable}
Replacing each element in the multiplication table for \(H\) by the image of
that element under \(\phi\) yields the table of \(\phi(xy)\) for all \(x,y\in
H\), which is shown in the margin. Using the multiplication table for \(G\), we
can construct the table of \(\phi(x)\cdot\phi(y)\) for all \(x,y\in H\), which
appears in the margin.

Since the last two tables agree in every position, \(\phi(xy) = \phi(x)\cdot
\phi(y)\) for all \(x,y\in H\), and since \(\phi\) is also a one-to-one
correspondence from \(H\) to \(G\), \(\phi\) is an isomorphism.

\vspace{2ex}\index{r@\(\mathbb{R}\)!group|(}
\noindent\textbf{12.}\quad Let \(G\) be the additive group of all real numbers,
and let \(G'\) be the group of all positive real numbers under multiplication.
Verify that the mapping \(\phi\colon G\to G'\) defined by \(\phi(x)=10^x\) is an
isomorphism from \(G\) to \(G'\).
\begin{proof}[\(\phi\colon(\mathbb{R}, +)\to(\mathbb{R}^+, \cdot)\) is an
  isomorphism]
  The proof contains three subproofs: that \(\phi\) is onto, that \(\phi\) is
  one-to-one, and that \(\phi\) has the morphism property.
  \begin{proof}[\(\phi\) is onto]
    Let \(y\in\mathbb{R}^+\). Then there exists a corresponding element \(x\in
    \mathbb{R}\) such that \(x=\log_{10}y\), and
    \begin{equation*}
      \phi(x) = \phi(\log_{10}y) = 10^{\log_{10}y} = y.
    \end{equation*}
    Since this holds for an arbitrary \(y\) in the codomain \(\mathbb{R}^+\),
    this holds for every element in the codomain, and it follows that every
    element in the codomain is the image of at least one element of the domain.
    Therefore \(\mathbb{R}^+=\phi(\mathbb{R})\), and \(\phi\) is onto.
  \end{proof}
  \begin{proof}[\(\phi\) is one-to-one]
    Let \(x_1,x_2\in\mathbb{R}\) such that \(\phi(x_1)=\phi(x_2)\). Then
    \begin{alignat*}{3}
      && \phi(x_1) &= \phi(x_2) &&\\
      \implies && 10^{x_1} &= 10^{x_2} &&\quad\text{by the definition of
      \(\phi\),}\\
      \implies && \log_{10}(10^{x_1}) &= \log_{10}(10^{x_2}) &&\quad
      \text{since \(\log_{10}\) is well-defined,}\\
      \implies && x_1\log_{10}10 &= x_2\log_{10}10 &&\quad\text{by a property
      of \(\log\), and}\\
      \implies && x_1 &= x_2 &&\quad\text{since \(\log_{10}10=1\).}
    \end{alignat*}
    So \(\phi(x_1) = \phi(x_2)\implies x_1 = x_2\), and it follows that
    different elements of the domain always have different images under
    \(\phi\), therefore \(\phi\) is one-to-one.
  \end{proof}
  \marginnote{Since \(\phi\) is both onto and one-to-one, \(\phi\) is a
  one-to-one correspondence from \(\mathbb{R}\) to \(\mathbb{R}^+\).}
  \begin{proof}[\(\phi\) has the morphism property]
    Let \(x,y\in\mathbb{R}\). Then
    \begin{alignat*}{2}
      \phi(x)\cdot\phi(y)&=10^x\cdot10^y&&\quad\text{by the definition of
      \(\phi\),}\\
      &= 10^{x+y}&&\quad\text{by an exponent law,}\\
      &= \phi(x + y)&&\quad\text{by the definition of \(\phi\).}
    \end{alignat*}
    Therefore for all \(x,y\in\mathbb{R}\), \(\phi(x+y)=\phi(x)\cdot\phi(y)\),
    so \(\phi\) preserves the operations of the two groups.
  \end{proof}

  Since \(\phi\) is a one-to-one correspondence from \(\mathbb{R}\) to
  \(\mathbb{R}^+\) and \(\phi\) has the morphism property, \(\phi\colon
  (\mathbb{R},+)\to(\mathbb{R}^+,\cdot)\) is an isomorphism.
\renewcommand{\qedsymbol}{$\blacksquare$}\end{proof}
\index{r@\(\mathbb{R}\)!group|)}

\index{automorphism|(}
\noindent\textbf{18.}\quad For each \(a\) in the group \(G\), define a mapping
\(t_a\colon G\to G\) by \(t_a(x) = axa^{-1}\). Prove that \(t_a\) is an
automorphism of \(G\).\marginnote{\begin{defn}
  An \emph{automorphism} is an isomorphism from a group to itself.
\end{defn}}
\begin{proof}[\(t_a\colon(G,\cdot)\to(G,\cdot)\) is an automorphism]
  Again, the proof contains three parts.
  \begin{proof}[\(t_a\) is onto]
    Let \(y\in G\). Since \(G\) is closed, there exists an element \(x\in G\)
    such that \(x=a^{-1}ya\), and
    \begin{alignat*}{2}
      t_a(x) &= t_a(a^{-1}ya)&&\\
      &= a(a^{-1}ya)a^{-1}&&\quad\text{by the definition of \(t_a\),}\\
      &= (aa^{-1})y(aa^{-1})&&\quad\text{since the operation is associative,
      and}\\
      &= y&&\quad\text{by the definition of inverses}.
    \end{alignat*}
    Therefore \(t_a\) is onto.
  \end{proof}
  \begin{proof}[\(t_a\) is one-to-one]
    Let \(x_1,x_2\in G\) such that \(t_a(x_1) = t_a(x_2)\). Then
    \begin{alignat*}{3}
      && t_a(x_1) &= t_a(x_2) &&\\
      \implies && ax_1a^{-1} &= ax_2a^{-1}&&\quad\text{by the definition of
      \(t_a\),}\\
      \implies && ax_1a^{-1}a &= ax_2a^{-1}a&&\quad\text{multiplying by
      \(a\),}\\
      \implies && ax_1 &= ax_2 &&\quad\text{since \(a^{-1}a=e\),}\\
      \implies && a^{-1}ax_1 &= a^{-1}ax_2&&\quad\text{multiplying by
      \(a^{-1}\), and}\\
      \implies && x_1 &= x_2 &&\quad\text{since \(a^{-1}a=e\)}.
    \end{alignat*}
    Therefore \(t_a\) is one-to-one.
  \end{proof}

  Since \(t_a\) is onto and one-to-one, \(t_a\) is a one-to-one correspondence
  from \(G\) to itself.
  \begin{proof}[\(t_a\) has the morphism property]
    Let \(x,y\in G\). Then
    \begin{alignat*}{2}
      t_a(x)t_a(y) &= (axa^{-1})(aya^{-1})&&\quad\text{by the definition of
      \(t_a\),}\\
      &= ax(a^{-1}a)ya^{-1}&&\quad\text{the operation is associative,}\\
      &= axya^{-1}&&\quad\text{since \(a^{-1}a = e\), and}\\
      &= t_a(xy)&&\quad\text{by the definition of \(t_a\).}
    \end{alignat*}
    Therefore the mapping \(t_a\) preserves the group's operation.
  \end{proof}

  The mapping \(t_a\) is a one-to-one correspondence that preserves the group
  operation, therefore \(t_a\) is an isomorphism. Since \(t_a\) maps \(G\) to
  itself, \(t_a\) is an automorphism.
\renewcommand{\qedsymbol}{$\blacksquare$}\end{proof}
\index{automorphism|)}
