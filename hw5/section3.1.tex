% !TEX root = ../portfolio.tex
\vspace{2ex}
\index{z@\(\mathbb{Z}\)!group|(}
\noindent\textbf{16.}\quad Determine whether \(\mathbb{Z}\) is a group, and
whether it is an abelian group,\marginnote{\begin{defn}
	If \((G,\ast)\) is a group and \(\forall x,y\in G,\enspace x\ast y=y\ast x\),
	then \(G\) is a \emph{commutative} or \emph{abelian} group.
\end{defn}}\index{group!abelian} with respect to the binary operation \(\ast\)
defined on \(\mathbb{Z}\) by the following rule: \(x\ast y = x + y - 1\).

The set \(\mathbb{Z}\) is closed under the operation \(\ast\); if
\(x,y\in\mathbb{Z}\), then \(x + y - 1\in\mathbb{Z}\), so \(x\ast
y\in\mathbb{Z}\) for all \(x\) and \(y\) in \(\mathbb{Z}\). To test whether
\(\ast\) is associative:
\begin{align*}
	x\ast(y\ast z) &= (x\ast y)\ast z\\
	x\ast(y + z - 1) &= (x + y - 1)\ast z\\
	x + (y + z - 1) - 1 &= (x + y - 1) + z - 1\\
	x + y + z - 2 &= x + y + z - 2.
\end{align*}
Both sides are the same, so \(\ast\) is associative. To find an identity element
\(e\), we seek an \(e\in\mathbb{Z}\) such that \(e\ast x = x\ast e = x\) for all
\(x\in\mathbb{Z}\). We apply the definition of \(\ast\),
\begin{equation*}
	e + x - 1 = x + e - 1 = x,
\end{equation*}
then subtract \((x - 1)\) to obtain \(e = 1\). Since
\(1\ast x = x\ast 1 = x\) for all \(x\in\mathbb{Z}\), \((\mathbb{Z}, \ast)\)
has the identity element \(1\).

If \((\mathbb{Z}, \ast)\) contains inverses, then for each \(a\in\mathbb{Z}\)
there exists \(b\in\mathbb{Z}\) such that \(a\ast b = b\ast a = e\). Using the
definition of \(\ast\) and the identity \(e = 1\), \(a + b - 1 = b + a - 1 =
1\). Subtracting \((a - 1)\) yields \(b = 2 - a\). So for each
\(a\in\mathbb{Z}\), there exists an inverse \(2-a\in\mathbb{Z}\), so
\((\mathbb{Z}, \ast)\) contains inverses.

Since \((\mathbb{Z}, \ast)\) is closed, \(\ast\) is associative, and
\((\mathbb{Z}, \ast)\) contains an identity element and inverses, \((\mathbb{Z},
\ast)\) is a group.

For \((\mathbb{Z}, \ast)\) to be an abelian group, \(\ast\) must be commutative,
that is, \(x\ast y = y\ast x\) for all \(x,y\in\mathbb{Z}\). Now
\begin{alignat*}{2}
	x\ast y &= x + y - 1&&\quad\text{by the definition of }\ast,\\
	&= y + x - 1&&\quad\text{since addition is commutative in }\mathbb{Z},\\
	&= y\ast x&&\quad\text{by the definition of }\ast.
\end{alignat*}
Therefore \((\mathbb{Z}, \ast)\) is an abelian group.
\index{z@\(\mathbb{Z}\)!group|)}

\index{z@\(\mathbb{Z}_n\)!group|(}
\noindent\textbf{22.}\quad \hypertarget{3.1.22}{Decide} whether the set
\(\{[1],[2],[3],[4]\}\subseteq\mathbb{Z}_5\) is a group with respect to
multiplication. If it is not a group, state all of the conditions of the
definition that fail to hold. If it is a group, state its order.

\begin{margintable}
	\centering
	\begin{tabular}{c|cccc}
	\(\cdot\) & \([1]\) & \([2]\) & \([3]\) & \([4]\)\\
		\hline
		\([1]\) & \([1]\) & \([2]\) & \([3]\) & \([4]\)\\
		\([2]\) & \([2]\) & \([4]\) & \([1]\) & \([3]\)\\
		\([3]\) & \([3]\) & \([1]\) & \([4]\) & \([2]\)\\
		\([4]\) & \([4]\) & \([3]\) & \([2]\) & \([1]\)
	\end{tabular}
\end{margintable}
The group's multiplication table is shown in the margin. The group is
\emph{closed}, every element in the table is in the group's set. By a
theorem\index{z@\(\mathbb{Z}_n\)!multiplication}\marginnote{\begin{thm}
	The rule for multiplication in \(\mathbb{Z}_n\), \([a][b]=[ab]\), defines a
	binary operation that is
	\begin{enumerate}[nolistsep]
		\item associative,\\
		\(\forall a,b,c\in\mathbb{Z}_n\enspace [a]([b][c])=([a][b])[c]\),
		\item commutative,\\
		\(\forall a,b\in\mathbb{Z}_n\enspace [a][b]=[b][a]\),
		\item and under this multiplication \(\mathbb{Z}_n\) has the identity
		element \([1]\).
	\end{enumerate}
\end{thm}}, multiplication is
associative in \(\mathbb{Z}_n\), therefore multiplication is associative in this
group. The group contains an \emph{identity element}, \([1]\), since the row
beside \([1]\) and the column under \([1]\) match the table's headings. The
group also contains \emph{inverses}, since the identity \([1]\) appears in every
column and every row. Furthermore, the group is \emph{commutative}, since the
table is symmetric with respect to its main diagonal.

As this is an (abelian) group, the problem requires that we state its
order.\marginnote{\begin{defn}
	The \emph{order} of a group is the number of elements that it contains.
\end{defn}\index{group!order}}
The group's elements are sets. Though these sets (numbers congruent to 1, 2, 3,
and 4 modulo 5) are infinite, the group consists of only four elements, so its
order is four.\index{z@\(\mathbb{Z}_n\)!group|)}

\vspace{2ex}
\index{group!quaternion|(}
\noindent\textbf{28.}\quad Write out the multiplication table for the quaternion
group.\marginnote{\begin{defn}
	The \emph{quaternion group} is \(G=\{1,i,j,k,-1,-i,-j,-k\}\) with
	noncommutative multiplication defined by
	\begin{align*}
		(-1)^2 &= 1,\\
		i^2 &= j^2 = k^2 = -1,\\
		ij &= -ji = k,\\
		jk &= -kj = i,\\
		ki &= -ik = j,\text{ and}\\
		-x &= (-1)x = x(-1)\enspace\forall x\in G.
	\end{align*}
\end{defn}}
\begin{center}
	\begin{tabular}{c|cccccccc}
\(\cdot\) & \(1\) & \(i\) & \(j\) & \(k\) & \(-1\) & \(-i\) & \(-j\) & \(-k\)\\
		\hline
		\(1\) & \(1\) & \(i\) & \(j\) & \(k\) & \(-1\) & \(-i\) & \(-j\) & \(-k\)\\
		\(i\) & \(i\) & \(-1\) & \(k\) & \(-j\) & \(-i\) & \(1\) & \(-k\) & \(j\)\\
		\(j\) & \(j\) & \(-k\) & \(-1\) & \(i\) & \(-j\) & \(k\) & \(1\) & \(-i\)\\
		\(k\) & \(k\) & \(j\) & \(-i\) & \(-1\) & \(-k\) & \(-j\) & \(i\) & \(1\)\\
		\(-1\) & \(-1\) & \(-i\) & \(-j\) & \(-k\) & \(1\) & \(i\) & \(j\) & \(k\)\\
		\(-i\) & \(-i\) & \(1\) & \(-k\) & \(j\) & \(i\) & \(-1\) & \(k\) & \(-j\)\\
		\(-j\) & \(-j\) & \(k\) & \(1\) & \(-i\) & \(j\) & \(-k\) & \(-1\) & \(i\)\\
		\(-k\) & \(-k\) & \(j\) & \(i\) & \(1\) & \(k\) & \(j\) & \(-i\) & \(-1\)
	\end{tabular}
\end{center}
\index{group!quaternion|)}

\index{power set!group|(}
\noindent\textbf{36.}\quad For an arbitrary set \(A\), the power set
\(\mathscr{P}(A)\) was defined in section 1.1 by \(\mathscr{P}(A)=\{X\mid
X\subseteq A\}\), and addition in \(\mathscr{P}(A)\) was defined
by\index{set!symmetric difference}\footnote{This operation is usually referred
to as the ``symmetric difference'' or ``disjunctive union''.}
\begin{align*}
	X + Y &= (X \cup Y)\smallsetminus(X \cap Y)\\
	&= (X\smallsetminus Y)\cup(Y\smallsetminus X).
\end{align*}

\textbf{a.}\quad Prove that \(\mathscr{P}(A)\) is a group
with respect to this operation of addition.
\begin{proof}[\((\mathscr{P}(A), +)\) is a group]
	The proof consists of the following four parts.
	\begin{proof}[Closure]
		For any \(X,Y\in\mathscr{P}(A)\), since \(X\subseteq A\) and \(Y\subseteq
		A\), the sum \(X + Y\) will contain only elements of \(A\), so \(X + Y\) is
		one of the possible subsets of \(A\), that is, \(X + Y\in\mathscr{P}(A)\).
		For this reason, \(\mathscr{P}(A)\) is closed under this addition.
	\end{proof}
	\begin{proof}[\hypertarget{3.1.36a.associative}{Associativity}] To simplify
	the proof, note that for arbitrary subsets \(A\) and \(B\) of a universal set
	\(U\), the complement of \(B\) in \(A\) can be expressed as follows:
		\begin{alignat*}{2}
			A\smallsetminus B &= \{x\mid x\in A\text{ and }x\notin B\}
			&&\quad\text{by def.\ of complement,}\\
			&= \{x\mid (x\in A\text{ and }x\in U)\text{ and }x\notin B\}
			&&\quad\text{by def.\ of subset,}\\
			&= \{x\mid x\in A\text{ and }(x\in U\text{ and }x\notin B)\}
			&&\quad\text{`and' is associative,}\\
			&= \{x\mid x\in A\text{ and }x\in\overline{B}\}
			&&\quad\text{by def.\ of complement,}\\
			&= A\cap\overline{B}&&\quad\text{by def.\ of intersection.}
		\end{alignat*}
		\marginnote{See \hyperlink{1.1.40b}{exercise 40 part \textbf{b} in section
		1.1} for a proof of the associativity of this operation by Venn diagrams,
		and see \hyperlink{review2.4.associative}{problem 4 in the review for test
		2} for a proof utilizing the characteristic function of a set.}
		This provides an equivalent definition of addition in \(\mathscr{P}(A)\)
		that does not involve set difference; since \(A\smallsetminus B = A\cap
		\overline{B}\) and intersection is commutative,
		\begin{equation*}
			X + Y = (X\smallsetminus Y)\cup(Y\smallsetminus X)
			= \left(X\cap\overline{Y}\right)\cup\left(Y\cap\overline{X}\right)
			=\left(X\cap\overline{Y}\right)\cup\left(\overline{X}\cap Y\right).
		\end{equation*}
		We aim to show that for any \(X,Y,Z\in\mathscr{P}(A)\), \(X+(Y+Z)=(X+Y)+Z\).
		Beginning on the left side, using the modified definition of addition:
		\begin{align*}
			X+(Y+Z)&=X+\left(\left(Y\cap\overline{Z}\right)\cup\left(\overline{Y}\cap
			Z\right)\right)\\
			&=\left(X\cap\overline{\left(Y\cap\overline{Z}\right)
			\cup\left(\overline{Y}\cap Z\right)}\right)\cup\left(\overline{X}\cap
			\left(\left(Y\cap\overline{Z}\right)\cup\left(\overline{Y}\cap
			Z\right)\right)\right)\\
			&=\left(X\cap\overline{Y\cap\overline{Z}}\cap\overline{\overline{Y}\cap
			Z}\right)\cup\left(\left(\overline{X}\cap Y\cap\overline{Z}\right)\cup
			\left(\overline{X}\cap\overline{Y}\cap Z\right)\right)\\
			&=\left(X\cap\left(\overline{Y}\cup Z\right)\cap\left(Y\cup
			\overline{Z}\right)\right)\cup\left(\overline{X}\cap Y\cap
			\overline{Z}\right)\cup\left(\overline{X}\cap\overline{Y}\cap Z\right)\\
			&=\left(X\cap\left(\left(\overline{Y}\cap\left(Y\cup
			\overline{Z}\right)\right)\cup\left(Z\cap\left(Y\cup
			\overline{Z}\right)\right)\right)\right)\cup\left(\overline{X}\cap Y\cap
			\overline{Z}\right)\cup\left(\overline{X}\cap\overline{Y}\cap Z\right)\\
			&=\left(X\cap\left(\cancel{\left(\overline{Y}\cap Y\right)}\cup
			\left(\overline{Y}\cap\overline{Z}\right)\cup\left(Z\cap Y\right)\cup
			\cancel{\left(Z\cap\overline{Z}\right)}\right)\right)\cup
			\left(\overline{X}\cap Y\cap\overline{Z}\right)\cup\left(\overline{X}\cap
			\overline{Y}\cap Z\right)\\
			&=\left(X\cap\left(\left(\overline{Y}\cap\overline{Z}\right)\cup
			\left(Z\cap Y\right)\right)\right)\cup\left(\overline{X}\cap Y\cap
			\overline{Z}\right)\cup\left(\overline{X}\cap\overline{Y}\cap Z\right)\\
			&=\left(X\cap\overline{Y}\cap\overline{Z}\right)\cup\left(X\cap Z\cap
			Y\right)\cup\left(\overline{X}\cap Y\cap\overline{Z}\right)\cup
			\left(\overline{X}\cap\overline{Y}\cap Z\right)\\
			&=\left(X\cap\overline{Y}\cap\overline{Z}\right)\cup\left(\overline{X}\cap
			Y\cap\overline{Z}\right)\cup\left(\overline{X}\cap\overline{Y}\cap
			Z\right)\cup\left(X\cap Z\cap Y\right)\\
			&=\left(X\cap\overline{Y}\cap\overline{Z}\right)\cup\left(\overline{X}\cap
			Y\cap\overline{Z}\right)\cup\left(\left(\overline{X}\cap
			\overline{Y}\right)\cap Z\right)\cup\left(\left(Y\cap X\right)\cap
			Z\right)\\
			&=\left(X\cap\overline{Y}\cap\overline{Z}\right)\cup\left(\overline{X}\cap
			Y\cap\overline{Z}\right)\cup\left(\left(\left(\overline{X}\cap
			\overline{Y}\right)\cup\left(Y\cap X\right)\right)\cap Z\right)\\
			&=\left(X\cap\overline{Y}\cap\overline{Z}\right)\cup\left(\overline{X}\cap
			Y\cap\overline{Z}\right)\cup\left(\left(\left(\overline{X}\cap
			X\right)\cup\left(\overline{X}\cap\overline{Y}\right)\cup\left(Y\cap
			X\right)\cup\left(Y\cap\overline{Y}\right)\right)\cap Z\right)\\
			&=\left(X\cap\overline{Y}\cap\overline{Z}\right)\cup\left(\overline{X}\cap
			Y\cap\overline{Z}\right)\cup\left(\left(\left(\overline{X}\cap\left(X\cup
			\overline{Y}\right)\right)\cup\left(Y\cap\left(X\cup
			\overline{Y}\right)\right)\right)\cap Z\right)\\
			&=\left(X\cap\overline{Y}\cap\overline{Z}\right)\cup\left(\overline{X}\cap
			Y\cap\overline{Z}\right)\cup\left(\left(\overline{X}\cup Y\right)\cap
			\left(X\cup\overline{Y}\right)\cap Z\right)\\
			&=\left(\left(X\cap\overline{Y}\cap\overline{Z}\right)\cup
			\left(\overline{X}\cap Y\cap\overline{Z}\right)\right)\cup
			\left(\overline{X\cap\overline{Y}}\cap\overline{\overline{X}\cap
			Y}\cap Z\right)\\
			&=\left(\left(\left(X\cap\overline{Y}\right)\cup\left(\overline{X}\cap
			Y\right)\right)\cap\overline{Z}\right)\cup\left(\overline{\left(X\cap
			\overline{Y}\right)\cup\left(\overline{X}\cap Y\right)}\cap Z\right)\\
			&=\left(\left(X\cap\overline{Y}\right)\cup\left(\overline{X}\cap
			Y\right)\right)+Z\\
			&=(X+Y)+Z.
		\end{align*}
		Therefore the operation \(+\) is associative in \(\mathscr{P}(A)\).
	\end{proof}
	\begin{proof}[Identity element]
		The identity element in \(\mathscr{P}(A)\), with respect to addition, is the
		empty set \(\varnothing\). Since union and intersection are commutative,
		\begin{equation*}
			X + \varnothing = \left(X\cup\varnothing\right)\smallsetminus\left(X\cap
			\varnothing\right)
			=\left(\varnothing\cup X\right)\smallsetminus\left(\varnothing\cap
			X\right)
			=\varnothing + X,
		\end{equation*}
		furthermore, the intersection of an empty set and another set is empty, the
		complement of the empty set in any set is the set itself, and the union of
		the empty set and any set is, again, the set itself, so
		\begin{equation*}
			X+\varnothing =\left(X\cup\varnothing\right)\smallsetminus\left(X\cap
			\varnothing\right)
			=\left(X\cup\varnothing\right)\smallsetminus\varnothing
			=X\cup\varnothing
			=X.
		\end{equation*}
		The power set of any set \(A\), \(\mathscr{P}(A)\), has the additive
		identity \(\varnothing\).
	\end{proof}
	\begin{proof}[Inverses]
		To identify the inverse, say \(Y\), of an arbitrary \(X\in\mathscr{P}(A)\),
		note that \(X + Y = Y + X\) always holds due to the commutativity of set
		union and set intersection, and suppose that \(X + Y = \varnothing\). Now
		\begin{alignat*}{3}
			&&X+Y&=\varnothing&&\\
			\iff&&\left(X\smallsetminus Y\right)\cup\left(Y\smallsetminus X\right)
			&=\varnothing&&\quad\text{Definition of addition}\\
			\iff&&X\smallsetminus Y=\varnothing\text{ and }Y\smallsetminus X
			&=\varnothing&&\quad\text{union is empty iff each are empty}\\
			\iff&&X\subseteq Y\text{ and }Y&\subseteq X&&\\
			\iff&&X&=Y.&&
		\end{alignat*}
		Therefore each element of \(\mathscr{P}(A)\) is its own inverse under
		addition.
	\end{proof}
For any set \(A\), the power set \(\mathscr{P}(A)\) is closed under addition,
this addition is associative, and \(\mathscr{P}(A)\) contains an identity
element and inverses for every one of its elements with respect to addition,
therefore \((\mathscr{P}(A), +)\) is a group.
\renewcommand{\qedsymbol}{$\blacksquare$}
\end{proof}

\index{power set!number of elements|(}
\textbf{b.}\quad If \(A\) has \(n\) distinct elements, state the order of
\(\mathscr{P}(A)\).

\noindent The order of \(\mathscr{P}(A)\) is the number of elements that it
contains. We recall\footnote{See \hyperlink{1.1.10a}{exercise 10 in section 1.1}
and \hyperlink{2.2.36}{exercise 36 in section 2.2}.} that
\begin{equation*}
	|A| = n \implies |\mathscr{P}(A)| = 2^n,
\end{equation*}
so if \(A\) contains \(n\) distinct elements, the order of \(\mathscr{P}(A)\) is
\(2^n\).
\index{power set!number of elements|)}

\vspace{2ex}
\noindent\textbf{38.}\quad Let \(A = \{a, b, c\}\). Prove or disprove that
\(\mathscr{P}(A)\) is a group with respect to the operation of union.

The set \(\mathscr{P}(A)\) is not a group with respect to union because it does
not contain inverses.
\begin{proof}[\((\mathscr{P}(A), \cup)\) is not a group]
	The set \(\mathscr{P}(A)\) does contain an identity with respect to the
	operation of union; for every \(X\in\mathscr{P}(A)\), it is clearly true that
	\(X\cup\varnothing = \varnothing\cup X = X\).

	However, if we assume that for some \(X,Y\in\mathscr{P}(A)\), \(X\cup Y =
	Y\cup X = \varnothing\), then it must be the case that \(X=Y=\varnothing\),
	that is, the only element of \(\mathscr{P}(A)\) that has an inverse is the
	empty set. Since not every element of \(\mathscr{P}(A)\) has an inverse,
	\((\mathscr{P}(A), \cup)\) is not a group.
\end{proof}
\index{power set!group|)}
