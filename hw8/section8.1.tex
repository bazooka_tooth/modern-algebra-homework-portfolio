% !TEX root = ../portfolio.tex
\marginnote{\begin{defn}
  A polynomial over a commutative ring with unity, \(R\), is one whose
  coefficients are elements of the ring \(R\). The set of all such polynomials
  with \(x\) as their indeterminate is denoted by \(R[x]\).
\end{defn}}
\noindent\textbf{4.}\quad Consider each of the following polynomials over
\(\mathbb{Z}_9\), where \(a\) is written for \([a]\) in \(\mathbb{Z}_9\):
\begin{equation*}
  f(x) = 2x^3 + 7x + 4,\quad g(x) = 4x^2 + 4x + 6,\quad h(x) = 6x^2 + 3.
\end{equation*}
Find each of the following polynomials with all coefficients in
\(\mathbb{Z}_9\).

For all parts of this problem, let \(f(x) = \sum_{i=0}^3a_ix^i\), \(g(x) =
\sum_{i=0}^2b_ix^i\), and \(h(x) = \sum_{i=0}^2c_ix^i\). As the coefficients
in these polynomials are integers modulo 9, all sums and/or products of these
coefficients will be computed as least positive residues modulo \(9\).

\textbf{a.}\quad \(f(x) + g(x)\).

\noindent Replacing omitted zero coefficients,
\begin{equation*}
  f(x) = 4x^0 + 7x^1 + 0x^2 + 2x^3\quad\text{and}\quad
  g(x) = 6x^0 + 4x^1 + 4x^2 + 0x^3.
\end{equation*}
\index{polynomials over a ring!addition}\marginnote{\begin{defn}
  Addition of polynomials in \(R[x]\) is defined by
  \begin{equation*}
    f(x) + g(x) = \sum_{i=0}^k(a_i + b_i)x^i,
  \end{equation*}
  where \(a_i\) and \(b_i\) are the coefficients of the \(i^{\text{th}}\) terms
  in \(f\) and \(g\), respectively.
\end{defn}}
Applying the definition of addition,
\begin{align*}
  f(x) + g(x) &= \sum_{i=0}^3(a_i + b_i)x^i\\
  &= (4+6)x^0+(7+4)x^1+(0+4)x^2+(2+0)x^3\\
  &= 1x^0+2x^1+4x^2+2x^3\\
  &= 2x^3 + 4x^2 + 2x + 1.
\end{align*}

\textbf{b.}\quad \(g(x) + h(x)\).

\noindent Again, we first replace omitted zero coefficients to obtain
\begin{equation*}
  g(x) = 6x^0 + 4x^1 + 4x^2\quad\text{and}\quad
  h(x) = 3x^0 + 0x^1 + 6x^2.
\end{equation*}
Now applying the definition of addition, we find
\begin{align*}
  g(x) + h(x) &= \sum_{i=0}^2(b_i + c_i)x^i\\
  &= (6+3)x^0 + (4+0)x^1 + (4+6)x^2\\
  &= 0x^0 + 4x^1 + 1x^2\\
  &= x^2 + 4x.
\end{align*}

\textbf{c.}\quad \(f(x)g(x)\).

\index{polynomials over a ring!multiplication}\marginnote{\begin{defn}
  Multiplication of polynomials in \(R[x]\) is defined by
  \begin{equation*}
    f(x)g(x) = \sum_{i=0}^{n+m}c_ix^i,\text{ where }c_i
    = \sum_{j=0}^ia_jb_{i-j},
  \end{equation*}
  and where \(a_i\) and \(b_i\) are the coefficients of the \(i^{\text{th}}\)
  terms in \(f\) and \(g\), respectively.
\end{defn}}
\noindent By the definition of multiplication, and since the degrees of \(f\)
and \(g\) are \(3\) and \(2\), respectively,
\begin{equation*}
  f(x)g(x) = \sum_{i=0}^5d_ix^i\quad\text{where}\quad
  d_i = \sum_{j=0}^ia_jb_{i-j}.
\end{equation*}
Expanding each \(d_i\) we have
\begin{align*}
  d_0 &= a_0b_0 = 4\cdot6 = 24 = 6,\\
  d_1 &= a_0b_1 + a_1b_0 = 4\cdot4 + 7\cdot6 = 16 + 42 = 58 = 4,\\
  d_2 &= a_0b_2 + a_1b_1 + a_2b_0 = 4\cdot4 + 7\cdot4 + 0\cdot6 = 16 + 28 + 0
  = 44 = 8,\\
  d_3 &= a_1b_2 + a_2b_1 + a_3b_0 = 7\cdot4 + 0\cdot4 + 2\cdot6 = 28 + 0 + 12
  = 40 = 4,\\
  d_4 &= a_2b_2 + a_3b_1 = 0\cdot4 + 2\cdot4 = 0 + 8 = 8,\quad\text{and}\\
  d_5 &= a_3b_2 = 2\cdot4 = 8,\quad\text{so}
\end{align*}
\begin{align*}
  f(x)g(x) &= \left(4x^0 + 7x^1 + 0x^2 + 2x^3\right)\left(6x^0 + 4x^1 +
  4x^2\right)\\
  &= 6x^0 + 4x^1 + 8x^2 + 4x^3 + 8x^4 + 8x^5\\
  &= 8x^5 + 8x^4 + 4x^3 + 8x^2 + 4x + 6.
\end{align*}

\textbf{d.}\quad \(g(x)h(x)\).

\noindent By the definition of multiplication, and since the degrees of \(g\)
and \(h\) are both two,
\begin{equation*}
  g(x)h(x) = \sum_{i=0}^4e_ix^i\quad\text{where}\quad
  e^i = \sum_{j=0}^ib_jc_{i-j}.
\end{equation*}
Expanding each \(e_i\) we have
\begin{align*}
  e_0 &= b_0c0 = 6\cdot3 = 18 = 0,\\
  e_1 &= b_0c_1 + b_1c_0 = 6\cdot0 + 4\cdot3 = 0 + 12 = 3,\\
  e_2 &= b_0c_2 + b_1c_1 + b_2c_0 = 6\cdot6 + 4\cdot0 + 4\cdot3 = 36 + 12
  = 48 = 3,\\
  e_3 &= b_1c_2 + b_2c_1 = 4\cdot6 + 4\cdot0 = 24 = 6,\quad\text{and}\\
  e_4 &= b_2c_2 = 4\cdot6 = 24 = 6,\quad\text{so}
\end{align*}
\begin{align*}
  g(x)h(x) &= \left(6x^0 + 4x^1 + 4x^2\right)\left(3x^0 + 0x^1 + 6x^2\right)\\
  &= 0x^0 + 3x^1 + 3x^2 + 6x^3 + 6x^4\\
  &= 6x^4 + 6x^3 + 3x^2 + 3x.
\end{align*}

\textbf{e.}\quad \(f(x)g(x) + h(x)\).

\noindent Using \(f(x)g(x)\) from part \textbf{c} and replacing omitted zero
coefficients in \(h(x)\), we find that
\begin{align*}
  f(x)g(x) &= 6x^0 + 4x^1 + 8x^2 + 4x^3 + 8x^4 + 8x^5\quad\text{and}\\
  h(x) &= 3x^0 + 0x^1 + 6x^2 + 0x^3 + 0x^4 + 0x^5,
\end{align*}
and by the definition of addition, where \(k_i\) are the coefficients in
\(f(x)g(x)\), we have
\begin{align*}
  f(x)g(&x) + h(x) = \sum_{i=0}^5\left(h_i + k_i\right)x^i\\
  &= (6+3)x^0 + (4+0)x^1 + (8+6)x^2 + (4+0)x^3 + (8+0)x^4 + (8+0)x^5\\
  &= 0x^0 + 4x^1 + 5x^2 + 4x^3 + 8x^4 + 8x^5\\
  &= 8x^5 + 8x^4 + 4x^3 + 5x^2 + 4x.
\end{align*}

\textbf{f.}\quad \(f(x) + g(x)h(x)\).

\noindent Using \(g(x)h(x)\) from part \textbf{d} and replacing some omitted
zero coefficients in \(f(x)\), we have
\begin{align*}
  f(x) &= 4x^0 + 7x^1 + 0x^2 + 2x^3 + 0x^4\quad\text{and}\\
  g(x)h(x) &= 0x^0 + 3x^1 + 3x^2 + 6x^3 + 6x^4,
\end{align*}
By the definition of addition, where \(m_i\) are the coefficients of
\(g(x)h(x)\), we have
\begin{align*}
  f(x) + g(x)h(x) &= \sum_{i=0}^4(a_i + m_i)x^i\\
  &= (4+0)x^0 + (7+3)x^1 + (0+3)x^2 + (2+6)x^3 + (0+6)x^4\\
  &= 4x^0 + 1x^1 + 3x^2 + 8x^3 + 6x^4\\
  &= 6x^4 + 8x^3 + 3x^2 + x + 4.
\end{align*}

\textbf{g.}\quad \(f(x)g(x) + f(x)h(x)\).

\noindent We haven't computed \(f(x)h(x)\) yet. By the definition of
multiplication, and since the degrees of \(f\) and \(h\) are 3 and 2,
respectively,
\begin{equation*}
  f(x)h(x) = \sum_{i=0}^5n_ix^i\quad\text{where}\quad
  n_i = \sum_{j=0}^ia_ic_{i-j}.
\end{equation*}
Expanding each \(n_i\) we have
\begin{align*}
  n_0 &= a_0c_0 = 4\cdot3 = 12 = 3,\\
  n_1 &= a_0c_1 + a_1c_0 = 4\cdot0 + 7\cdot3 = 0 + 21 = 3,\\
  n_2 &= a_0c_2 + a_1c_1 + a_2c_0 = 4\cdot6 + 7\cdot0 + 0\cdot3 = 24 = 6,\\
  n_3 &= a_1c_2 + a_2c_1 + a_3c_0 = 7\cdot6 + 0\cdot0 + 2\cdot3 = 42 + 6 = 48
  = 3,\\
  n_4 &= a_2c_2 + a_3c_1 = 0\cdot6 + 2\cdot0 = 0,\quad\text{and}\\
  n_5 &= a_3c_2 = 2\cdot6 = 12 = 3,\quad\text{so}
\end{align*}
\begin{align*}
  f(x)h(x) &= \left(4x^0 + 7x^1 + 0x^2 + 2x^3\right)\left(3x^0 + 0x^1
  + 6x^2\right)\\
  &= 3x^0 + 3x^1 + 6x^2 + 3x^3 + 0x^4 + 3x^5.
\end{align*}
From part \textbf{c} we have that
\begin{equation*}
  f(x)g(x) = 6x^0 + 4x^1 + 8x^2 + 4x^3 + 8x^4 + 8x^5,
\end{equation*}
and by the definition of addition, where \(p_i\) and \(q_i\) are the
coefficients in \(f(x)g(x)\) and \(f(x)h(x)\), respectively,
\begin{align*}
  f(x)g(&x) + f(x)h(x) = \sum_{i=0}^5(p_i + q_i)x^i\\
  &= (6+3)x^0 + (4+3)x^1 + (8+6)x^2 + (4+3)x^3 + (8+0)x^4 + (8+3)x^5\\
  &= 0x^0 + 7x^1 + 5x^2 + 7x^3 + 8x^4 + 2x^5\\
  &= 2x^5 + 8x^4 + 7x^3 + 5x^2 + 7x.
\end{align*}

\textbf{h.}\quad \(f(x)h(x) + g(x)h(x)\).

\noindent Using \(f(x)h(x)\) from part \textbf{g} and \(g(x)h(x)\) from part
\textbf{d}, and replacing one zero coefficient in the latter,
\begin{align*}
  f(x)h(x) &= 3x^0 + 3x^1 + 6x^2 + 3x^3 + 0x^4 + 3x^5,\quad\text{and}\\
  g(x)h(x) &= 0x^0 + 3x^1 + 3x^2 + 6x^3 + 6x^4 + 0x^5.
\end{align*}
Now by the definition of addition, where \(r_i\) and \(s_i\) are the
two sets of coefficients,
\begin{align*}
  f(x)h(&x) + g(x)h(x) = \sum_{i=0}^5(r_i + s_i)x^i\\
  &= (3+0)x^0 + (3+3)x^1 + (6+3)x^2 + (3+6)x^3 + (0+6)x^4 + (3+0)x^5\\
  &= 3x^0 + 6x^1 + 0x^2 + 0x^3 + 6x^4 + 3x^5\\
  &= 3x^5 + 6x^4 + 6x + 3.
\end{align*}

\index{ring!unity}\index{integral domain}
\marginnote{See the definition of `unity' \hyperlink{test2review.5}{in problem
5} and the definition of `integral domain' \hyperlink{test2review.12}{in problem
12} in the review for the second test, in the ninth chapter of this document.}
\noindent\textbf{10.}\quad Let \(R\) be a commutative ring with unity. Prove
that
\begin{equation*}
  \operatorname{deg}(f(x)g(x)) \leq \operatorname{deg}f(x) +
  \operatorname{deg}g(x)
\end{equation*}
for all nonzero \(f(x)\), \(g(x)\) in \(R[x]\), even if \(R\) is not an
integral domain.

\begin{proof}
  Let \(R\) be a commutative ring with unity, and suppose that
  \begin{equation*}
    f(x) = \sum_{i=0}^na_ix^i\quad\text{and}\quad
    g(x) = \sum_{i=0}^nb_ix^i
  \end{equation*}
  are elements of \(R[x]\), with \(\operatorname{deg}f(x)=n\) and
  \(\operatorname{deg}g(x)=m\). Then
  \begin{equation*}
    f(x)g(x) = \sum_{i=0}^{n+m}c_ix^i\quad\text{where}\quad
    c_i = \sum_{j=0}^ia_jb_{i-j}
  \end{equation*}
  by the definition of multiplication, so the greatest degree of any term,
  \emph{regardless of whether the coefficient of that term is zero}\footnote{The
  fact that this \(R\) is a commutative ring with unity but \emph{not} an
  integral domain means that this \(R\) may contain zero divisors. The degree of
  a polynomial is the largest integer \(k\) such that the coefficient of \(x^k\)
  \emph{is nonzero}; if \(R\) contains zero divisors then the product of the
  coefficients of \(x^n\) and \(x^m\) in \(f\) and \(g\) may be zero. Only in
  this case is the degree of the product less than the sum of the degrees of
  \(f\) and \(g\).}, is \(n+m\). Therefore
  \begin{equation*}
    \operatorname{deg}(f(x)g(x))\leq n+m = \operatorname{deg}f(x) +
    \operatorname{deg}g(x).
  \end{equation*}
\end{proof}
