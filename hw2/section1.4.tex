% !TEX root = ../portfolio.tex
\marginnote{\begin{defn}
	A \emph{binary operation} on a set \(A\) is a mapping from \(A\times A\) to
	\(A\).
\end{defn}}\index{binary operation!definition and properties}
\hypertarget{1.4.6}{\noindent\textbf{6.}\quad} Let \(S\) be the set of four
elements given by \(S = \{A, B, C, D\}\) with the table at the right.
% the following two commands draw a diagonal dotted line in a table
\newcommand{\tikzmark}[2][]{\tikz[remember picture, overlay]\node[#1,inner
	sep=0pt,xshift=-\tabcolsep](#2){};\ignorespaces}
\newcommand{\tm}[2][]{\tikzmark[yshift=#1]{#2}}
\begin{margintable}
	\centering
	\begin{tabular}{c| c c c c}
		\tm[1.4ex]{A}\(\ast\) & \(A\) & \(B\) & \(C\) & \(D\)\\
		\hline
		\(A\) & \(A\) & \(A\) & \(A\) & \(A\)\\
		\(B\) & \(A\) & \(B\) & \(A\) & \(B\)\\
		\(C\) & \(A\) & \(A\) & \(C\) & \(C\)\\
		\(D\) & \(A\) & \(B\) & \(C\) & \(D\)\tm[.7ex]{B}
	\end{tabular}
	\tikz[remember picture, overlay]\draw[densely dotted] (A)--(B);
\end{margintable}
\marginnote{The table above is a \emph{Cayley table}. (The following chapters
reference this document.) These were used to
investigate groups in the \hyperlink{test1unfinished.3}{first test} and in
\hyperlink{3.1.22}{exercise 22} in the sixth chapter. Cayley tables are used in
the context of isomorphism in \hyperlink{3.5.10}{exercise 10} of the seventh
chapter and \hyperlink{test2review.13}{problem 13} in the review for the second
test. These tables are also used to treat rings in
\hyperlink{test2review.9}{problem 9} and fields in
\hyperlink{test2review.16}{problem 16} in the ninth chapter.}

\textbf{a.}\quad Is the binary operation \(\ast\) commutative? Why?
\marginnote{\begin{defn}
	A binary operation \(\ast\) on a set \(A\) is \emph{commutative} if
	\(\forall x,y\in A\enspace x\ast y = y\ast x\).
\end{defn}}

\noindent The table shown to the right is a \emph{Cayley table}.\index{Cayley
table} Although we could check for commutativity by examining every
\(x \ast y\)/\(y \ast x\) pair, this table offers a shortcut.

The operation associated with a Cayley table is commutative if and only if the
table's content is symmetric along the main diagonal, which is indicated here by
the dotted line. The given table does have this symmetry, so we can conclude
that the operation is commutative.

\vspace{2ex}
\textbf{b.}\quad Determine whether there is an identity element in \(S\) for
\(\ast\).
\marginnote{\begin{defn}
	An element \(e\) of a set \(A\) under a binary operation \(\ast\) is an
	\emph{identity element} if \(\forall x\in A\enspace e\ast x = x\ast e = x\).
\end{defn}}

\noindent We can locate the identity element in a Cayley table by searching for
a row \emph{and} column that both extend from the same element (which will be
\(e\)) in the table headers, and that both contain the same elements, in the
same order, as the headers. In this case, that element is \(D\).

\vspace{2ex}
\textbf{c.}\quad If there is an identity element, which elements have inverses?
\marginnote{\begin{defn}
	Given \(a\in A\), if there exists a \(b\in A\) s.t.\ \(a\ast b=b\ast a=e\),
	then \(a\) and \(b\) are \emph{inverses}.
\end{defn}}

\noindent In this table, the identity element \(D\) appears only once, where it
represents that \(D \ast D = D\). Therefore the only invertible element is
\(D\), and \(D\) is its own two-sided inverse.

\vspace{2ex}
\noindent\textbf{8.}\quad Prove or disprove that the set of all odd integers is
closed with respect to addition.
\marginnote{\begin{defn}
	A set is \emph{closed} under an operation if performance of that operation on
	members of the set always produces a member of the same set.
\end{defn}}

The set of all odd integers, \(\mathbb{Z}\smallsetminus \mathbb{E}\), is closed
with respect to addition only if \(x + y\in \mathbb{Z}\smallsetminus\mathbb{E}\)
for all \(x,y\in \mathbb{Z}\smallsetminus \mathbb{E}\). This is not the case,
which we will prove by counterexample.
\begin{proof}[Counterexample]
	Observe that \(1\in \mathbb{Z}\smallsetminus \mathbb{E}\), but \(1 + 1 = 2\)
	and \(2\notin \mathbb{Z}\smallsetminus \mathbb{E}\). Therefore \(\mathbb{Z}
	\smallsetminus \mathbb{E}\) is not closed  with respect to addition.
\end{proof}
