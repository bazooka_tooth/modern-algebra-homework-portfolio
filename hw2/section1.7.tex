% !TEX root = ../portfolio.tex
\noindent\textbf{4.}\quad Let \(R\) be the relation `congruence modulo 5'
\index{congruence modulo|(}
defined on \(\mathbb{Z}\) as follows: \(x\) is congruent to \(y\) modulo 5 if
and only if \(x - y\) is a multiple of 5, and we write \(x\equiv y \pmod{5}\).
\marginnote{\begin{defn}
	A \emph{relation} \(R\) on a set \(A\) is a subset of \(A\times A\). If \((x,
	y)\in R\), we write \(xRy\).
\end{defn}}\index{relation!definition}

\textbf{a.}\quad Prove that `congruence modulo 5' is an equivalence relation.
\marginnote{\begin{defn}
	An \emph{equivalence relation} \(R\) on a set \(A\) is a
	relation that, \(\forall x,y,z\in A\), possesses the following properties:
	\begin{enumerate}
		\item (Reflexivity) \(xRx\),
		\item (Symmetry) \(xRy\implies yRx\), and
		\item (Transitivity) \(xRy\land yRx\implies xRz\).
	\end{enumerate}
\end{defn}}\index{relation!equivalence relation}

\begin{proof}
	\hypertarget{eqreln}{First} we show that \(R\) is reflexive. Let
	\(x\in\mathbb{Z}\). Then \(x - x = 0\) and zero is a multiple of five. In
	other words, \(x\equiv x \pmod{5}\) for all integers \(x\), so \(xRx\) and
	\(R\) is reflexive.

	Next we show that \(R\) is symmetric. Where \(x,y\in\mathbb{Z}\), assume
	\(xRy\). Then \(x - y\) is a multiple of five, so there exists an integer
	\(k\) such that \(x - y = 5k\). Now
	\begin{equation*}
		y - x = -(x - y) = -(5k) = 5(-k).
	\end{equation*}
	Because \(k\in\mathbb{Z}\), \((-k)\in\mathbb{Z}\), hence \(y - x\) is a
	multiple of five and \(yRx\). Therefore \(xRy\implies yRx\) and \(R\) is
	symmetric.

	Finally, we show that \(R\) is transitive. Assume that \(xRy\) and \(yRz\) for
	some \(x,y,z\in\mathbb{Z}\). Then \(x - y\) and \(y - z\) are multiples of
	five, which implies the existence of integers \(k\) and \(l\) such that
	\begin{equation*}
		x - y = 5k\text{ and }y - z = 5l.
	\end{equation*}
	Adding these equations, we obtain
	\begin{equation*}
		x - z = 5k + 5l = 5(k + l).
	\end{equation*}
	Since \(k,l\in\mathbb{Z}\), \(k + l\in\mathbb{Z}\), thus \(x - z\) is a
	multiple of five and we have \(x\equiv y\pmod{5}\), so \(xRz\) and \(R\) is
	transitive. We have shown that \(R\) is reflexive, symmetric, and transitive,
	so by definition \(R\) is an equivalence relation.
\end{proof}

\textbf{b.}\quad List five members of each of the equivalence classes \([0]\),
\([1]\), \([2]\), \([8]\), and \([-4]\).
\marginnote{\begin{defn}
	Let \(R\) be an equivalence relation on a set \(A\). For each \(a\in A\), the
	set \([a]=\{x\in A\mid xRa\}\) is the equivalence class containing \(a\).
\end{defn}}\index{equivalence class}

Since \(x\equiv a\pmod{5}\) (i.e. \(xRa\)) if and only if \(5|(x - a)\), the
equivalence class \([a]\) contains all integers that differ from \(a\) by a
multiple of five. Therefore
\begin{align*}
	[a] &= \{\,5x + a\text{ for all }x\in\mathbb{Z}\,\},\text{ so}\\
	[0] &= \{\,\dotsc, -10, -5, 0, 5, 10, \dotsc\,\},\\
	[1] &= \{\,\dotsc, -9, -4, 1, 6, 11, \dotsc\,\},\\
	[2] &= \{\,\dotsc, -8, -3, 2, 7, 12, \dotsc\,\},\\
	[8] &= \{\,\dotsc, -2, 3, 8, 13, 18, \dotsc\,\},\text{ and}\\
	[-4] &= \{\,\dotsc, -14, -9, -4, 1, 6, \dotsc\,\}.
\end{align*}
\index{congruence modulo|)}

\noindent\textbf{24.}\quad Let \(\mathscr{L} = \{\,\alpha,\beta,\gamma\,\}\),
\(A_\alpha = \{\,1, 2, 3,\dotsc\,\}\), \(A_\beta = \{\,-1, -2, -3,\dotsc\,
\}\), and \(A_\gamma = \{\,0\,\}\). Write out \(\bigcup_{\lambda\in\mathscr{L}}
A_\lambda\) and \(\bigcap_{\lambda\in\mathscr{L}} A_\lambda\).
\marginnote{\begin{defn}
	Let \(\{A_\lambda\}\), \(\lambda\in\mathscr{L}\), be a collection of subsets
	of a set \(A\). Then \(\{A_\lambda\}\) is a \emph{partition} of \(A\) if:
	\begin{enumerate}
		\item Each \(A_\lambda\) is nonempty,
		\item \(A=\bigcup_{\lambda\in\mathscr{L}}A_\lambda\), and
		\item if \(A_\alpha\cap A_\beta\neq\varnothing\), then \(A_\alpha=A_\beta\).
	\end{enumerate}
\end{defn}}\index{set!partition}\index{set!collection}

In this problem we are considering a collection of sets \(\{\,A_\lambda\,\}\)
indexed by symbols \(\lambda\in\mathscr{L}\). The union and intersection of a
collection of sets are defined, respectively, by
\begin{align*}
	\bigcup_{\lambda\in\mathscr{L}} A_\lambda &= \{\,x\mid x\in A_\lambda
	\text{ for at least one } \lambda\in\mathscr{L}\,\},\text{ and}\\
	\bigcap_{\lambda\in\mathscr{L}} A_\lambda &= \{\,x\mid x\in A_\lambda
	\text{ for every } \lambda\in\mathscr{L}\,\}.
\end{align*}
Here,
\begin{align*}
	\bigcup_{\lambda\in\mathscr{L}} A_\lambda &= \{\,1, 2, 3,\dotsc\,\}\cup
	\{\,-1, -2, -3,\dotsc\,\}\cup\{\,0\,\} = \mathbb{Z},\text{ and}\\
	\bigcap_{\lambda\in\mathscr{L}} A_\lambda &= \{\,1, 2, 3,\dotsc\,\}\cap
	\{\,-1, -2, -3,\dotsc\,\}\cap\{\,0\,\} = \varnothing.
\end{align*}

This collection \(\{\,A_\lambda\,\}\) is a partition of \(\mathbb{Z}\) because
it satisfies condition 1 and condition 2, as well as condition 3 (vacuously) of
the definition.
