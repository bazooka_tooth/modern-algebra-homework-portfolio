% !TEX root = ../portfolio.tex
\vspace{2ex}
\index{z@\(\mathbb{Z}\)!postulates|(}
\index{order relation}
\noindent\textbf{1.} part \textbf{(i)}\quad Assuming all postulates for the set
of integers \(\mathbb{Z}\),\marginnote{See the postulates for \(\mathbb{Z}\) at
\hyperlink{zpostulates}{the beginning of the third chapter} of this text.}
prove (using postulates) that for every \(x,y\in\mathbb{Z}\), \(x < y \implies
a + x < a + y\).
\begin{proof}
  Let \(x,y\in\mathbb{Z}\) with \(x<y\). By the definition of `less than',
  \marginnote{See the definition of the order relation in
  \hyperlink{orderrelation}{section 2.1} in the third chapter of this text.}
  \begin{equation*}
    x < y \iff y + (-x)\in\mathbb{Z}^+.
  \end{equation*}
  Let \(a\in\mathbb{Z}\), then there exists an additive inverse of \(a\)
  (i.e.\ \(-a\)) such that \(a + (-a) = 0\). Now
  \begin{alignat*}{3}
    y + (-x) &= y + (-x) + 0&&\quad\text{zero is the additive identity}\\
    &= y + (-x) + [a + (-a)]&&\quad\text{since \(0 = a + (-a)\)}\\
    &= y + (-x) + a + (-a)&&\quad\text{addition is associative}\\
    &= a + y + (-a) + (-x)&&\quad\text{addition is commutative}\\
    &= a + y + [-(a + x)]&&\quad\text{by the distributive law}\\
    &= (a + y) + [-(a + x)]&&\quad\text{addition is associative.}
  \end{alignat*}
  Since \(y + (-x)\in\mathbb{Z}^+\) and \(y + (-x) = (a + y) + [-(a + x)]\),
  it must be the case that \((a + y) + [-(a + x)]\in\mathbb{Z}^+\). By the
  definition of `less than',
  \begin{equation*}
    (a + y) + [-(a + x)]\in\mathbb{Z}^+\iff a + x < a + y.
  \end{equation*}
\end{proof}
\noindent\textbf{1.} part \textbf{(ii)}\quad Assuming all postulates for the set
of integers \(\mathbb{Z}\), prove (using postulates) that for every
\(x\in\mathbb{Z}\), \(x\cdot0=0\).
\begin{proof}
  Let \(x\in\mathbb{Z}\). Then
  \begin{alignat*}{3}
    x\cdot0 + x\cdot0 &= 0\cdot x + 0\cdot x&&\quad\text{(multiplication is
    commutative)}\\
    &= (0 + 0)\cdot x&&\quad\text{by the distributive law}\\
    &= 0\cdot x&&\quad\text{zero is the additive identity element}\\
    &= x\cdot0&&\quad\text{multiplication is commutative.}
  \end{alignat*}
  Let \(y\) denote \(x\cdot 0\), so we have that \(y + y = y\). Since
  \(x\in\mathbb{Z}\) and \(0\in\mathbb{Z}\), by the closure of \(\mathbb{Z}\)
  under multiplication \(x\cdot 0\in\mathbb{Z}\) therefore \(y\in\mathbb{Z}\),
  and we may apply the postulates for the integers to the statement \(y + y =
  y\).
  \begin{alignat*}{4}
    &&y + y &= y&&\\
    \implies&&-y + (y + y) &= -y + y&&\quad\text{by the existence of additive
    inverses}\\
    \implies&&(-y + y) + y &= -y + y&&\quad\text{addition is associative}\\
    \implies&&0 + y &= 0&&\quad\text{by the definition of additive inverse}\\
    \implies&&y&=0&&\quad\text{zero is the additive identity element}\\
    \therefore&&x\cdot0&=0&&\quad\text{we have let \(y = x\cdot0\).}
  \end{alignat*}
\end{proof}
\index{z@\(\mathbb{Z}\)!postulates|)}
\index{Cayley table|(}
\noindent\textbf{3.} part \textbf{(a)}\quad
\hypertarget{test1unfinished.3}{Construct} addition and multiplication tables
for \(\mathbb{Z}_4\), the set of congruence classes modulo four.
\begin{margintable}
  \centering
  \begin{tabular}{c|cccc}
      \(+\) & \([0]\) & \([1]\) & \([2]\) & \([3]\)\\
    \hline
    \([0]\) & \([0]\) & \([1]\) & \([2]\) & \([3]\)\\
    \([1]\) & \([1]\) & \([2]\) & \([3]\) & \([0]\)\\
    \([2]\) & \([2]\) & \([3]\) & \([0]\) & \([1]\)\\
    \([3]\) & \([3]\) & \([0]\) & \([1]\) & \([2]\)
  \end{tabular}
\end{margintable}
\begin{margintable}
  \centering
  \begin{tabular}{c|cccc}
  \(\cdot\) & \([0]\) & \([1]\) & \([2]\) & \([3]\)\\
    \hline
    \([0]\) & \([0]\) & \([0]\) & \([0]\) & \([0]\)\\
    \([1]\) & \([0]\) & \([1]\) & \([2]\) & \([3]\)\\
    \([2]\) & \([0]\) & \([2]\) & \([0]\) & \([2]\)\\
    \([3]\) & \([0]\) & \([3]\) & \([2]\) & \([1]\)
  \end{tabular}
\end{margintable}

The tables are shown in the margin.

\index{z@\(\mathbb{Z}_n\)!group|(}
\vspace{2ex}
\noindent\textbf{3.} part \textbf{(b)}\quad Determine whether the algebraic
structures with the given binary operations \((\mathbb{Z}_4, +)\) and
\((\mathbb{Z}_4, \cdot)\) are groups.

Using the addition table,\marginnote{The use of these Cayley tables is
introduced in \hyperlink{1.4.6}{exercise 6 of section 1.4}.} we can see that
\((\mathbb{Z}_4, +)\) has the following properties:
\begin{itemize}[nolistsep]
  \item \(\mathbb{Z}_4\) is closed under \(+\); every element in the addition
  table is also an element of \(\mathbb{Z}_4\).
  \item \((\mathbb{Z}_4, +)\) has the identity element \([0]\); the row and
  column under the headings \([0]\) contain the same elements, in the same
  order, as their respective parallel headings.
  \item \((\mathbb{Z}_4, +)\) contains inverses; the identity element \([0]\)
  appears in every row and every column in the table.
\end{itemize}
Using the multiplication table, we can see that \((\mathbb{Z}_4, \cdot)\) has
the following properties:
\begin{itemize}[nolistsep]
  \item \(\mathbb{Z}_4\) is closed under \(\cdot\); every element in the
  multiplication table is also an element of \(\mathbb{Z}_4\).
  \item \((\mathbb{Z}_4, \cdot)\) has the identity element \([1]\); the row and
  column under the headings \([1]\) contain the same elements, in the same
  order, as their respective parallel headings.
\end{itemize}
However, \((\mathbb{Z}_4, \cdot)\) does not contain an inverse element for
\([0]\) or \([2]\);
the identity element \([1]\) does not appear in the column or row under the
headings \([0]\) or \([2]\). Because of this, \emph{\((\mathbb{Z}_4, \cdot)\) is
not a group.}

Now let's return to \((\mathbb{Z}_4, +)\). For it to be a group,\marginnote{See
the definition of a group in \hyperlink{groupdef}{the next section}.} its binary
operation \(+\) must be associative, that is, for every
\(x,y,z\in\mathbb{Z}_4\), it must be the case that
\begin{equation*}
  x + (y + z) = (x + y) + z.
\end{equation*}

\index{Cayley table!Light's associativity test}Light's associativity
test\cite{CliffordPreston1961} is an algorithm for verifying the associativity
of a group that directly involves the use of tables. This algorithm offers
improved speed when the generating set of the group is smaller than the group's
associated set. For \((\mathbb{Z}_4, +)\), the set \(\{[1]\}\) is a generating
set; every element of the group can be expressed by a statement involving only
\([1]\) and the group operation \(+\).

Light's algorithm involves the definition of new operators \(\ast\) and
\(\circ\) for the left and right sides of the associativity statement:
\begin{equation*}
  x\ast y = x + (a + y)\quad\text{and}\quad x\circ y = (x + a) + y,
\end{equation*}
where \(a\) is an element of the generating set. The algorithm then checks that
the results of each operation are the same for every \(a\). Here the generating
set consists of only one element, so we'll need only to check that the tables
corresponding to
\begin{equation*}
  x\ast y = x + ([1] + y)\quad\text{and}\quad x\circ y = (x + [1]) + y
\end{equation*}
are the same.

First we make a table whose top header is the \([1]\) row from the original
addition table, and whose contents are the columns from the original table that
correspond to the new header. Then the top header is deleted, and the new
headers are set as the originals. This process generates the first table in the
margin. The effect of the \(\ast\) operation is a left-shift by one column of
the contents of the addition table.
\begin{margintable}
  \centering
  \begin{tabular}{c|cccc}
      \(\ast ([1])\) & \([0]\) & \([1]\) & \([2]\) & \([3]\)\\
    \hline
    \([0]\) & \([1]\) & \([2]\) & \([3]\) & \([0]\)\\
    \([1]\) & \([2]\) & \([3]\) & \([0]\) & \([1]\)\\
    \([2]\) & \([3]\) & \([0]\) & \([1]\) & \([2]\)\\
    \([3]\) & \([0]\) & \([1]\) & \([2]\) & \([3]\)
  \end{tabular}
\end{margintable}

For the \(\circ\) operation, we make a table whose left header is the \([1]\)
column from the original addition table, and whose contents are the rows from
the original table that correspond to the new left header. Then the left header
is deleted, and the new headers are set as the originals. This generates the
second table in the margin. The effect of the \(\circ\) operation is an up-shift
by one row of the contents of the addition table.
\begin{margintable}
  \centering
  \begin{tabular}{c|cccc}
      \(\circ ([1])\) & \([0]\) & \([1]\) & \([2]\) & \([3]\)\\
    \hline
    \([0]\) & \([1]\) & \([2]\) & \([3]\) & \([0]\)\\
    \([1]\) & \([2]\) & \([3]\) & \([0]\) & \([1]\)\\
    \([2]\) & \([3]\) & \([0]\) & \([1]\) & \([2]\)\\
    \([3]\) & \([0]\) & \([1]\) & \([2]\) & \([3]\)
  \end{tabular}
\end{margintable}

The contents of both tables are the same, so the algorithm's result is that the
\(+\) operator is associative in \(\mathbb{Z}_4\). In this case, Light's
algorithm has reduced the problem by \(1/2\); instead of the original 64
equations we've generated tables that contain a total of 32 elements. However,
this depended on the knowledge of the generating set.

At any rate, \((\mathbb{Z}_4, +)\) is closed, its operator is associative, and
it contains an identity element and inverses, so \((\mathbb{Z}_4, +)\) is a
group.
\index{z@\(\mathbb{Z}_n\)!group|)}\index{Cayley table|)}
